#
# ~/.profile
#
# shellcheck shell=sh
# shellcheck disable=SC1090,SC2155

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DESKTOP_DIR="$HOME/Desktop"
export XDG_DOCUMENTS_DIR="$HOME/Documents"
export XDG_DOWNLOAD_DIR="$HOME/Downloads"
export XDG_MUSIC_DIR="$HOME/Music"
export XDG_PICTURES_DIR="$HOME/Pictures"
export XDG_VIDEOS_DIR="$HOME/Videos"
export XDG_PUBLICSHARE_DIR="$HOME/Public"
export XDG_TEMPLATES_DIR="$HOME/Templates"

export LESS=-R
export LESSHISTFILE=''
export LESS_TERMCAP_mb="$(printf '\033[1;31m')"
export LESS_TERMCAP_md="$(printf '\033[1;36m')"
export LESS_TERMCAP_me="$(printf '\033[0m')"
export LESS_TERMCAP_so="$(printf '\033[01;44;33m')"
export LESS_TERMCAP_se="$(printf '\033[0m')"
export LESS_TERMCAP_us="$(printf '\033[1;32m')"
export LESS_TERMCAP_ue="$(printf '\033[0m')"

export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
export LEDGER_FILE="$HOME/Sync/hledger/main.journal"

command -v dircolors >/dev/null 2>&1 && eval "$(dircolors)"
export LS_COLORS="${LS_COLORS:-'di=34:ln=35:so=32:pi=33:ex=31:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=30;43'}"

export FZF_DEFAULT_OPTS="--bind 'alt-/:toggle-preview,alt-d:preview-page-down,alt-u:preview-page-up'"
if command -v fd &>/dev/null; then
    export FZF_DEFAULT_COMMAND='fd -H'
    export FZF_CTRL_T_COMMAND='fd -H --exclude=.git --exclude=.cache'
    export FZF_ALT_C_COMMAND='fd -H --exclude=.git --exclude=.cache -t d'
fi

export PAGER=less
if [ -z "$DISPLAY" ]; then
    export BROWSER=links
fi

if command -v qt5ct >/dev/null; then
    export QT_QPA_PLATFORMTHEME="${QT_QPA_PLATFORMTHEME:-qt5ct}"
else
    export QT_QPA_PLATFORMTHEME="${QT_QPA_PLATFORMTHEME:-gtk2}"
fi

case "$XDG_CURRENT_DESKTOP" in
KDE)
    export QT_QPA_PLATFORMTHEME=''
    FILEMANAGER=dolphin
    TERMINAL=konsole
    ;;
GNOME)
    QT_QPA_PLATFORMTHEME="gnome"
    #QT_STYLE_OVERRIDE=kvantum
    if pgrep gnome-shell >/dev/null 2>&1; then
        FILEMANAGER=nautilus
        TERMINAL=kgx
    else
        FILEMANAGER=thunar
        TERMINAL=xfce4-terminal
    fi
    ;;
*)
    #    export GDK_SCALE=2
    ;;
esac

first_available() { for x; do command -v -- "${x%% *}" >/dev/null 2>&1 && echo "$x" && break; done; }
export TERMINAL="$(first_available "$TERMINAL" xfce4-terminal xterm alacritty konsole kgx gnome-terminal kitty urxvt tilix)"
export TERMCMD="$TERMINAL"
export FILEMANAGER="$(first_available "$FILEMANAGER" dolphin thunar pcmanfm spacefm nautilus)"
export BROWSER="$(first_available "$BROWSER" firefox chromium)"
export EDITOR="$(first_available "$EDITOR" vi nano ed)"
export VISUAL="$(first_available "$VISUAL" nvim kak vim nano vi)"
export GRAPHICAL="$(first_available "$GRAPHICAL" codium code-oss code)"

# if command -v emacs && ! pidof emacs; then emacs --daemon & fi >/dev/null 2>&1

_append_to_path() { for p in "$@"; do [ -d "$p" ] && case ":$PATH:" in *":$p:"*) : ;; *) PATH="$PATH:$p" ;; esac done }
_append_to_path \
    /usr/local/go/bin \
    "$HOME/bin" \
    "$HOME/.local/bin" \
    "$GOPATH/bin" \
    "$HOME/.emacs.d/bin"

[ -e ~/.profile.local ] && . ~/.profile.local
