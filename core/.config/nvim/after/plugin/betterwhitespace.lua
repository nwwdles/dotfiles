vim.opt.list = true
vim.keymap.set("n", "<leader>ts", function()
	vim.cmd.ToggleWhitespace()
	vim.cmd [[set list!]]
end)

