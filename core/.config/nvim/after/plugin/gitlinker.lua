require("gitlinker").setup({
	callbacks = {
		["stash.msk.avito.ru"] = function(url_data)
			local host = require"gitlinker.hosts".get_base_https_url(url_data)
			host = string.gsub(host, "^(.+)/(.+)/(.+)$", "%1/projects/%2/repos/%3/browse/")
			local url = host .. url_data.file .. "?at=" .. url_data.rev
			if url_data.lstart then
				url = url .. "#" .. url_data.lstart
				if url_data.lend then url = url .. "-" .. url_data.lend end
			end
			return url
		end
	},
})
