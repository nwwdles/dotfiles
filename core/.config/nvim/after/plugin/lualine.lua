local function cwd()
	return string.gsub(vim.fn.getcwd(), ".*/", "")
end

require('lualine').setup({
	options = { section_separators = '', component_separators = '' },
	sections = {
		lualine_b = { cwd, 'branch', 'diff', 'diagnostics' },
	}
})
