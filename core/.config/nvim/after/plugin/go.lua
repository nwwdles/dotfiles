require('go').setup({
	icons = { breakpoint = '🧘', currentpos = '🏃' }, -- setup to `false` to disable icons setup
	lsp_inlay_hints = { enable = false },
})

vim.keymap.set('n', '<leader>tb', vim.cmd.GoBreakToggle)
vim.keymap.set('n', '<leader>cl', vim.cmd.GoCodeLenAct)
