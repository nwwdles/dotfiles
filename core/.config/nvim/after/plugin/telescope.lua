local telescope = require("telescope")
local telescopeConfig = require("telescope.config")
local actions = require "telescope.actions"
local builtin = require('telescope.builtin')

local vimgrep_arguments = { unpack(telescopeConfig.values.vimgrep_arguments) }
table.insert(vimgrep_arguments, "--hidden")
table.insert(vimgrep_arguments, "--glob")
table.insert(vimgrep_arguments, "!**/.git/*")

telescope.setup({
	defaults = {
		vimgrep_arguments = vimgrep_arguments,
	},
	pickers = {
    buffers = {
	    mappings = {
		    i = {
			    ["<c-d>"] = actions.delete_buffer + actions.move_to_top,
		    }
	    }
    },
    find_files = {
	    find_command = {
		    "rg",
		    "--files",
		    "--hidden",
				"--glob",
				"!**/.git/*",
				"--glob",
				"!/vendor/*",
			},
		},
	},
})

local findfiles = function(...)
	vim.fn.system('git rev-parse --is-inside-work-tree')
	if vim.v.shell_error == 0 then
		return builtin.git_files(...)
	else
		return builtin.find_files(...)
	end
end

vim.keymap.set('n', '<leader> ', builtin.find_files, {})
vim.keymap.set('n', '<C-p>', findfiles, {})
vim.keymap.set('n', '<leader>ff', builtin.live_grep, {})
vim.keymap.set('n', '<leader>b', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
