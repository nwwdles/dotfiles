require("no-clown-fiesta").setup({
    transparent = true, -- Enable this to disable the bg color
})

vim.cmd[[colorscheme no-clown-fiesta]]
