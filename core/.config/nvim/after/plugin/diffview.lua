vim.keymap.set('n', '<leader>gd', function()
	if vim.t.diffview_view_initialized then
		return vim.cmd.DiffviewClose()
	end
	return vim.cmd.DiffviewOpen()
end, { silent = true })
