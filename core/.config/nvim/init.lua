require("plugins")

vim.opt.number = true
vim.opt.wrap = false
vim.opt.relativenumber = true
vim.opt.smartcase = true
vim.opt.ignorecase = true
vim.opt.breakindent = true
vim.opt.smartindent = true
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.cache/vim/undodir"
vim.opt.incsearch = true
vim.opt.scrolloff = 8
vim.opt.colorcolumn = "80"
vim.opt.langmap =
'ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯЖ;ABCDEFGHIJKLMNOPQRSTUVWXYZ:,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz'
vim.g.mapleader = ' '
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set('n', '<leader>fs', '<cmd>write<cr>')
vim.keymap.set('n', '<leader>fl', ':setfiletype ')
vim.keymap.set('n', '<leader>fS', function()
	vim.cmd.write();
	vim.cmd.so();
	vim.cmd.PackerSync();
end)
vim.keymap.set('n', '<leader>.', vim.cmd.Ex)
vim.keymap.set({ 'n', 'x' }, '<leader>y', '"+y')
vim.keymap.set({ 'n', 'x' }, '<leader>p', '"+p')
vim.keymap.set('n', '<leader>tw', ':set wrap!<cr>')
vim.keymap.set('n', "<leader>rc", ":luafile $MYVIMRC<CR>")
vim.keymap.set('n', "<leader>tt", function() vim.fn.jobstart('$TERMCMD') end)
vim.keymap.set('n', "<leader>gl", function()
	vim.cmd.terminal('lazygit');
	vim.cmd.startinsert();
end)
vim.keymap.set('n', "<leader>tT",
	function()
		local d = vim.fn.expand("%:p:h");
		vim.fn.jobstart('$TERMCMD', { cwd = d })
	end)

for _, mode in pairs({ 'n', 'i', 'v', 'x' }) do
	for _, key in pairs({ '<Up>', '<Down>', '<Left>', '<Right>' }) do
		vim.keymap.set(mode, key, '<nop>')
	end
end
vim.opt.listchars = "tab:| ,trail:␣,extends:>,precedes:<,space:␣"
vim.keymap.set("n", "<C-k>", "<cmd>cnext<CR>zz")
vim.keymap.set("n", "<C-j>", "<cmd>cprev<CR>zz")
vim.keymap.set("n", "<leader>k", "<cmd>lnext<CR>zz")
vim.keymap.set("n", "<leader>j", "<cmd>lprev<CR>zz")
vim.keymap.set("n", "<C-_>", "<cmd>Commands<CR>")
