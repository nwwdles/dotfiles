vim.cmd [[packadd packer.nvim]]

return require('packer').startup({
	function(use)
		use {
			'nvim-lualine/lualine.nvim',
			requires = { 'nvim-tree/nvim-web-devicons', opt = true }
		}
		use({
			'cappyzawa/trim.nvim',
			config = function()
				require("trim").setup({})
			end
		})
		use { 'kevinhwang91/nvim-ufo', requires = 'kevinhwang91/promise-async' }
		use 'jose-elias-alvarez/null-ls.nvim'
		use 'wbthomason/packer.nvim'
		use 'sindrets/diffview.nvim'
		use 'folke/trouble.nvim'
		use { 'codota/tabnine-nvim', run = "./dl_binaries.sh" }
		use 'mfussenegger/nvim-dap'
		use { "rcarriga/nvim-dap-ui", requires = { "mfussenegger/nvim-dap" } }
		use 'theHamsta/nvim-dap-virtual-text'
		use 'ray-x/go.nvim'
		use({
			'kylechui/nvim-surround',
			tag = "*", -- Use for stability; omit to use `main` branch for the latest features
			config = function()
				require("nvim-surround").setup({
					-- Configuration here, or leave empty to use defaults
				})
			end
		})
		use 'ray-x/guihua.lua' -- recommended if need floating window support
		use '/home/anon/src/github.com/nwwdles/no-clown-fiesta.nvim'
		use 'ishan9299/modus-theme-vim'
		use {
			'nvim-telescope/telescope.nvim',
			tag = '0.1.1',
			requires = { { 'nvim-lua/plenary.nvim' } }
		}
		use('nvim-treesitter/nvim-treesitter', { run = ':TsUpdate' })
		use('nvim-treesitter/playground')
		use('theprimeagen/harpoon')
		use('simrat39/symbols-outline.nvim')
		use('mg979/vim-visual-multi')
		use('mbbill/undotree')
		use('tpope/vim-fugitive')
		use {
			"folke/which-key.nvim",
			config = function()
				vim.o.timeout = true
				vim.o.timeoutlen = 300
				require("which-key").setup {
				}
			end
		}
		use {
			'nvim-telescope/telescope-file-browser.nvim',
			requires = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" }
		}
		use('ThePrimeagen/vim-be-good')
		use('ntpeters/vim-better-whitespace')
		use('lewis6991/gitsigns.nvim')
		use('rmagatti/auto-session')
		use('junegunn/fzf.vim')
		use {
			'numToStr/Comment.nvim',
		}
		use { 'kevinhwang91/nvim-bqf', ft = 'qf' }
		use {
			'ruifm/gitlinker.nvim',
			requires = 'nvim-lua/plenary.nvim',
		}
		use('rbong/vim-flog')
		-- use('aktersnurra/no-clown-fiesta.nvim')
		use {
			'VonHeikemen/lsp-zero.nvim',
			branch = 'v2.x',
			requires = {
				-- LSP Support
				{ 'neovim/nvim-lspconfig' }, -- Required
				{
					-- Optional
					'williamboman/mason.nvim',
					run = function()
						pcall(vim.cmd, 'MasonUpdate')
					end
				}, { 'williamboman/mason-lspconfig.nvim' }, -- Optional
				-- Autocompletion
				{ 'hrsh7th/nvim-cmp' }, -- Required
				{ 'hrsh7th/cmp-nvim-lsp' }, -- Required
				{ 'L3MON4D3/LuaSnip' } -- Required
			}
		}
	end,
	config = {}
})
