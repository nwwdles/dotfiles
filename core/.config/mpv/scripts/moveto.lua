local mp = require("mp")

function MoveFileTo()
    local r = mp.command_native({
        name = "subprocess",
        args = {"move-to-pics", mp.get_property("path")}
    })
    if r.status == 0 then mp.commandv("playlist-next") end
end

mp.add_key_binding("n", MoveFileTo)
