local mp = require("mp")

function OpenScreenshotFolder()
    local dir = mp.get_property("screenshot-directory")
    dir = dir:gsub("^~/?", os.getenv("HOME") .. '/')
    mp.command_native({name = "subprocess", args = {"xdg-open", dir}})
    return
end

mp.add_key_binding("Alt+e", OpenScreenshotFolder)

