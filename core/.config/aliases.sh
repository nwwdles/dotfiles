#
# aliases.sh
#
# shellcheck shell=sh

if [ -n "$RELOADING_CONFIG" ]; then
    echo "config reloaded"
    unset -v RELOADING_CONFIG
fi
rel() {
    export RELOADING_CONFIG=true
    if [ -n "$ZSH_VERSION" ]; then exec zsh; fi
    if [ -n "$BASH" ]; then exec bash; fi
    echo "rel doesn't support this shell"
}

alias cp="cp -i"
alias df='df -h'
alias du='du -h'
alias free='free -h'
alias grep='grep --color=auto'
alias less='less -S'
alias ls='ls --color=auto'
alias mkdir='mkdir -p'

alias c="$GRAPHICAL"
alias g=git
alias v="$VISUAL"
alias x=xdg-open
alias h='codium ~/Sync/hledger && hw'

alias cd-='cd -'
alias cx='chmod +x'
alias grba='git rebase --abort'
alias grbc='GIT_EDITOR=true git rebase --continue'
alias grbi='git rebase -i'
alias grbs='git rebase --skip'
alias l='ls -lhg'
alias ll='ls -Alhg'
alias mpvl="mpv --profile=low-latency"
alias mpvm="mpv --ytdl-format='bestvideo[height<=?720]+bestaudio/best'"
alias pf="printf '%s\n'"
alias ydl=youtube-dl
alias acurl='curl --key ~/.avito/certs/personal.key --cert ~/.avito/certs/personal.crt'

if command -v yt-dlp >/dev/null 2>&1; then
    alias ydl=yt-dlp
fi

md() { mkdir -p "$1" && cd "$1"; }

if command -v exa >/dev/null 2>&1; then
    alias l='exa -lhg'
    alias ll='exa -alhg'
    alias ls='exa'
    alias tree='exa --tree'
fi

if command -v pacman >/dev/null 2>&1; then
    alias y="yay"
    alias syu="sudo pacman -Syu"
else
    alias syu="sudo apt update && sudo apt upgrade"
fi
