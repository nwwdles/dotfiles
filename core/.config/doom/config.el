;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
(setq user-full-name "nwwdles"
      user-mail-address "nwwdles@gmail.com")

(setq doom-font (font-spec :family "monospace" :size 24 :weight 'regular)
      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 24)
      doom-big-font (font-spec :family "monospace" :size 40)
      doom-theme 'acme)

(setq display-line-numbers-type 'relative
      show-trailing-whitespace t
      fill-column 80)

(setq auto-mode-alist
      (append '(("\\.envrc\\'" . shell-script-mode)
                ("\\.xprofile\\'" . shell-script-mode)
                ("\\.profile\\'" . shell-script-mode))
              auto-mode-alist))

(use-package! acme-theme
  :config
  (setq acme-theme-black-fg t)
  :custom-face
  (sh-quoted-exec ((t (:foreground "black"))))
  (highlight-numbers-number ((t (:foreground "#880000"))))
  (font-lock-constant-face ((t (:foreground "#880000" :weight normal))))
  (font-lock-type-face ((t (:foreground "#000000" :weight normal))))
  (font-lock-keyword-face ((t (:foreground "black"))))
  (centaur-tabs-selected ((t (:foreground "#000000" :background "#FFFFE8"))))
  (centaur-tabs-selected-modified ((t (:foreground "#333333" :background "#FFFFE8"))))
  (centaur-tabs-unselected ((t (:foreground "#333333" :background "#E1FAFF"))))
  (centaur-tabs-unselected-modified ((t (:foreground "#000000" :background "#E1FAFF")))))

(define-generic-mode sxhkd-mode
  '(?#)
  '("alt" "Escape" "super" "bspc" "ctrl" "space" "shift")
  nil
  '("sxhkdrc" "sxhkdrc*")
  nil
  "Simple mode for sxhkdrc files.")

(after! yaml-mode
  (add-hook 'yaml-mode-hook
            '(lambda ()
               (define-key yaml-mode-map "\C-m" 'newline-and-indent))))

(after! org
  (setq org-directory "~/Documents/org")
  (setq org-todo-keywords
        '((sequence "TODO(t)" "DOING(i)" "HOLD(h)" "MAYBE(m)" "|" "DONE(d!)" "CANCELED(c)")
          (sequence "[ ](T)" "[-](S)" "[?](W)" "|" "[X](D)")))
  (setq org-todo-keyword-faces
        '(("[-]" . +org-todo-active)
          ("DOING" . +org-todo-active)
          ("[?]" . +org-todo-onhold)
          ("HOLD" . +org-todo-onhold)
          ("MAYBE" . +org-todo-onhold))))

(after! persp-mode
  (setq persp-emacsclient-init-frame-behaviour-override "main"))

(after! projectile
  (setq projectile-project-search-path '("~/src"))
  (defun projectile-extra-discover-projects-in-search-path ()
    (message "Searching for projects in %s..." projectile-project-search-path)
    (let ((dirs (seq-map 'file-name-directory (directory-files-recursively projectile-project-search-path "^.git$" t))))
      (seq-do 'projectile-add-known-project dirs)
      (message "Added %d projects" (length dirs))))
  (defun projectile-extra-discover-projects-in-subfolders (projects-root)
    (interactive (list (read-directory-name "Add to known projects: ")))
    (message "Searching for projects in %s..." projects-root)
    (let ((dirs (seq-map 'file-name-directory (directory-files-recursively projects-root "^.git$" t))))
      (seq-do 'projectile-add-known-project dirs)
      (message "Added %d projects" (length dirs)))))


(after! counsel
  (setq counsel-rg-base-command
        "rg -M 240 --hidden --with-filename --no-heading --line-number --color never -g '!.git/*' %s"))

(after! evil
  (cua-mode t)
  (setq evil-vsplit-window-right t
        evil-split-window-below t)
  (defadvice! prompt-for-buffer (&rest _)
    :after '(evil-window-split evil-window-vsplit)
    (+ivy/switch-buffer))
  (setq +ivy-buffer-preview t)
  (map! :map evil-window-map
        "SPC"          #'rotate-layout
        "C-<left>"     #'evil-window-left
        "C-<down>"     #'evil-window-down
        "C-<up>"       #'evil-window-up
        "C-<right>"    #'evil-window-right
        "<left>"       #'+evil/window-move-left
        "<down>"       #'+evil/window-move-down
        "<up>"         #'+evil/window-move-up
        "<right>"      #'+evil/window-move-right)
  (map! "C-/" #'evilnc-comment-or-uncomment-lines)
  (map! :map (evil-insert-state-map evil-normal-state-map evil-visual-state-map)
        "C-s"   #'save-buffer
        "C-z"   #'evil-undo
        "C-f"   #'evil-ex-search-forward
        "C-S-f" #'evil-ex-search-backward
        "C-S-z" #'evil-redo
        "C-S-c" #'cua-copy-region
        "C-S-x" #'cua-cut-region
        "C-v"   #'cua-paste))

(after! evil-mc
  (map! :map evil-visual-state-map
        "A"   #'evil-mc-make-cursor-in-visual-selection-end
        "I"   #'evil-mc-make-cursor-in-visual-selection-beg)
  (map! ;; :map (evil-insert-state-map evil-normal-state-map evil-visual-state-map)
   "M-d"   #'evil-mc-make-and-goto-next-match
   "M-S-d" #'evil-mc-make-and-goto-prev-match
   "M-f"   #'evil-mc-skip-and-goto-next-match
   "M-S-f" #'evil-mc-skip-and-goto-prev-match
   "M-z"   #'evil-mc-undo-last-added-cursor))

(after! centaur-tabs
  (remove-hook '+doom-dashboard-mode-hook #'centaur-tabs-local-mode)
  (remove-hook '+popup-buffer-mode-hook #'centaur-tabs-local-mode)
  (map!
   "C-<prior>" #'centaur-tabs-backward
   "C-<next>" #'centaur-tabs-forward)
  (defun centaur-tabs-buffer-groups ()
    "`centaur-tabs-buffer-groups' control buffers' group rules.

    Group centaur-tabs with mode if buffer is derived from `eshell-mode' `emacs-lisp-mode' `dired-mode' `org-mode' `magit-mode'.
    All buffer name start with * will group to \"Emacs\".
    Other buffer group by `centaur-tabs-get-group-name' with project name."
    (list
     (cond
      (t "General")))))

(use-package! term
  :config
  (setq vterm-shell "/usr/bin/env zsh"))

(use-package! flycheck-inline
  :config
  (global-flycheck-inline-mode t))

(use-package! reverse-im
  :custom
  (reverse-im-input-methods '("russian-computer"))
  :config
  (reverse-im-mode t))

(use-package! char-fold
  :custom
  (char-fold-symmetric t)
  (search-default-mode #'char-fold-to-regexp))

;; Here are some additional functions/macros that could help you configure Doom:
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.
