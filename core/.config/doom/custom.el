(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("4780d7ce6e5491e2c1190082f7fe0f812707fc77455616ab6f8b38e796cbffa9" "3e335d794ed3030fefd0dbd7ff2d3555e29481fe4bbb0106ea11c660d6001767" "835d934a930142d408a50b27ed371ba3a9c5a30286297743b0d488e94b225c5f" default))
 '(org-agenda-files '("~/Documents/org/todo.org")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(centaur-tabs-selected ((t (:foreground "#000000" :background "#FFFFE8"))))
 '(centaur-tabs-selected-modified ((t (:foreground "#333333" :background "#FFFFE8"))))
 '(centaur-tabs-unselected ((t (:foreground "#333333" :background "#E1FAFF"))))
 '(centaur-tabs-unselected-modified ((t (:foreground "#000000" :background "#E1FAFF"))))
 '(font-lock-constant-face ((t (:foreground "#880000" :weight normal))))
 '(font-lock-keyword-face ((t (:foreground "black"))))
 '(font-lock-type-face ((t (:foreground "#000000" :weight normal))))
 '(highlight-numbers-number ((t (:foreground "#880000"))))
 '(sh-quoted-exec ((t (:foreground "black")))))
