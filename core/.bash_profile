#
# ~/.bash_profile
#
# shellcheck shell=bash

#shellcheck source=.profile
[[ -f ~/.profile ]] && source ~/.profile

#shellcheck source=.bashrc
[[ -f ~/.bashrc ]] && source ~/.bashrc
