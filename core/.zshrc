#
# ~/.zshrc
#
# shellcheck shell=bash  # let's say bash is good enough
# shellcheck disable=SC2148,SC1090,SC2206,SC2034,SC2128,SC1087,SC2086

HISTSIZE=10000000
SAVEHIST=$HISTSIZE
HISTFILE=${ZDOTDIR:-$HOME}/.zsh_history
if [[ -e "$HOME/Sync" ]]; then HISTFILE=~/Sync/.zsh_history; fi

WORDCHARS='*_.=!^<>' # '*?_-.[]~=/&;!#$%^(){}<>'
KEYTIMEOUT=1         # no delay on ESC presses

# OPTIONS (http://zsh.sourceforge.net/Doc/Release/Options.html)
opts=(
    AUTO_CD
    ALWAYS_TO_END
    COMPLETE_IN_WORD
    EXTENDED_GLOB
    MAGIC_EQUAL_SUBST
    MARK_DIRS
    NUMERIC_GLOB_SORT
    RC_EXPAND_PARAM
    EXTENDED_HISTORY
    HIST_EXPIRE_DUPS_FIRST
    HIST_FIND_NO_DUPS
    HIST_IGNORE_ALL_DUPS
    HIST_IGNORE_DUPS
    HIST_IGNORE_SPACE
    HIST_REDUCE_BLANKS
    HIST_SAVE_NO_DUPS
    SHARE_HISTORY
    CORRECT
    INTERACTIVE_COMMENTS
    PATH_DIRS
    AUTO_CONTINUE
    PROMPT_SUBST
    BSD_ECHO
    nobanghist
)
unsetopts=(
    CASE_GLOB
    NOMATCH
    FLOW_CONTROL
    BEEP
)

_zsh_cfg_abbrev_alias() {
    while read -r a; do
        case "$a" in
        l=* | ls=* | ll=* | ll?=* | la=* | z?=* | z=* | cp=* | grep=*) ;;
        *) eval "abbrev-alias $a" ;;
        esac
    done < <(alias -r)
}

_zsh_cfg_autosuggestions() {
    # ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=3'
    ZSH_AUTOSUGGEST_USE_ASYNC=1
    ZSH_AUTOSUGGEST_STRATEGY=(
        history_smart
        completion
        # match_prev_cmd
    )
}

_zsh_autosuggest_strategy_history_smart() {
    emulate -L zsh
    setopt EXTENDED_GLOB

    local prefix="${1//(#m)[\\*?[\]<>()|^~#]/\\$MATCH}"
    local -a his=($history)
    local index=0
    local new_match
    local arg

    local match=$(
        while true; do
            index="$his[(i)${prefix}*]"
            new_match="$his[$index]"
            case $new_match in
            '') break ;;
            . | .. | ./) ;;
            ./* | ../* | ~/* | /*)
                if [ -e "$new_match" ]; then
                    echo $new_match
                    return
                fi
                ;;
            'cd '*)
                arg=${new_match#cd }
                case $arg in
                . | ..) ;;
                *)
                    if [ -d "$arg" ]; then
                        echo $new_match
                        return
                    fi
                    ;;
                esac
                ;;
            *)
                echo $new_match
                return
                ;;
            esac

            his=(${his[index + 1, -1]}) # discard tested history entries
        done
    )

    typeset -g suggestion="$match"
}

_zsh_cfg_history_substring_search() {
    bindkey '^[[A' history-substring-search-up
    bindkey '^[[B' history-substring-search-down
    bindkey -M emacs '^P' history-substring-search-up
    bindkey -M emacs '^N' history-substring-search-down
    bindkey '^[[1;2A' history-substring-search-up
    bindkey '^[[1;4A' history-substring-search-up
    bindkey '^[[1;7A' history-substring-search-up
    bindkey '^[^[[A' history-substring-search-up
    bindkey '^[[1;2B' history-substring-search-down
    bindkey '^[[1;4B' history-substring-search-down
    bindkey '^[[1;7B' history-substring-search-down
    bindkey '^[^[[B' history-substring-search-down
}

_zsh_zinit_init() {
    for f in \
        "${XDG_DATA_HOME}/zinit/bin/zinit.zsh" \
        "$HOME/.local/share/zinit/bin/zinit.zsh" \
        "$HOME/.zinit/bin/zinit.zsh" \
        "/usr/share/zsh/plugin-managers/zinit/zinit.zsh"; do
        if [[ ! -f $f ]]; then
            continue
        fi
        source "$f"
        autoload -Uz _zinit

        zinit wait lucid atload'!_zsh_cfg_abbrev_alias' for momo-lab/zsh-abbrev-alias
        zinit wait lucid atload'_zsh_cfg_history_substring_search' for zsh-users/zsh-history-substring-search

        zinit snippet OMZP::command-not-found
        zinit snippet OMZP::fzf
        zinit snippet OMZP::sudo
        zinit wait lucid atinit'_zsh_cfg_autosuggestions' atload'_zsh_autosuggest_start' for zsh-users/zsh-autosuggestions
        zinit wait lucid for mdumitru/fancy-ctrl-z

        zicompinit
        return
    done

    # if we reached here (notice the `return` above), zinit couldn't be found
    echo "zinit not installed. To install it, run '_zsh_zinit_install' and restart zsh"
}

_zsh_cfg_fast_syntax_highlighting() {
    unset 'FAST_HIGHLIGHT[chroma-awk]'
}

_zsh_zinit_install() {
    echo "Installing zinit. If something  goes wrong, refer to https://github.com/zdharma-continuum/zinit"
    mkdir -p "$HOME/.zinit"
    git clone "https://github.com/zdharma-continuum/zinit.git" "$HOME/.zinit/bin"
}

main() {
    _zsh_zinit_init

    autoload -Uz vcs_info
    zmodload zsh/complist # should be before keybinds; initializes keymap `menuselect'

    zstyle ':urlglobber' url-other-schema
    zstyle ':completion:*' list-colors "exfxcxdxbxegedabagacad"
    zstyle ':completion::complete:*' 'use-cache' on
    zstyle ':completion::complete:*' cache-path "${XDG_CACHE_HOME:-$HOME/.cache}/.zcompcache"
    zstyle ':completion:*' rehash true
    zstyle ':completion:*:functions' ignored-patterns '_*'
    zstyle ':completion:*:make:*' tag-order 'targets'
    zstyle ':completion:*:make:*:targets' call-command true
    zstyle ':completion:*' menu select=1
    zstyle ':compinstall' filename "${ZDOTDIR:-$HOME}/.zshrc"
    zstyle ':completion:*' accept-exact '*(N)'
    # fuzzy completion (https://superuser.com/questions/415650/does-a-fuzzy-matching-mode-exist-for-the-zsh-shell)
    zstyle ':completion:*' matcher-list '' \
        'm:{[:lower:]\-}={[:upper:]\_}' \
        'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{[:lower:]\-}={[:upper:]\_}' \
        'r:|?=** m:{[:lower:]\-}={[:upper:]\_}'

    setopt $opts
    unsetopt $unsetopts

    # keybinds
    bindkey -e
    bindkey '^[[1;2A' ''
    bindkey '^[[1;4A' ''
    bindkey '^[[1;7A' ''
    bindkey '^[^[[A' ''
    bindkey '^[[1;2B' ''
    bindkey '^[[1;4B' ''
    bindkey '^[[1;7B' ''
    bindkey '^[^[[B' ''
    bindkey '^[[1;2D' 'backward-word'
    bindkey '^[[1;4D' 'backward-word'
    bindkey '^[^[[D' 'backward-word'
    bindkey '^[[1;6D' 'backward-word'
    bindkey '^[[1;2C' 'forward-word'
    bindkey '^[[1;4C' 'forward-word'
    bindkey '^[^[[C' 'forward-word'
    bindkey '^[[1;6C' 'forward-word'
    bindkey '^[[5~' beginning-of-buffer-or-history # page up
    bindkey '^[[6~' end-of-buffer-or-history       # page down
    bindkey '^[[1;7D' beginning-of-line
    bindkey '^[[7~' beginning-of-line
    bindkey '^[[H' beginning-of-line
    bindkey '^[[1;7C' end-of-line
    bindkey '^[[8~' end-of-line
    bindkey '^[[F' end-of-line
    bindkey '^[[C' forward-char
    bindkey '^[^[[C' forward-char
    bindkey '^[[D' backward-char
    bindkey '^[^[[D' backward-char
    bindkey '^[Oc' forward-word
    bindkey '^[[1;3C' forward-word
    bindkey '^[[1;5C' forward-word
    bindkey '^[Od' backward-word
    bindkey '^[[1;3D' backward-word
    bindkey '^[[1;5D' backward-word
    bindkey '^[[3~' delete-char     # delete
    bindkey '^H' backward-kill-word # ctrl+backspace
    bindkey '^[z' undo              # alt + z
    bindkey '^[Z' redo              # alt + shift + z
    bindkey '^I' expand-or-complete
    bindkey '^[[Z' reverse-menu-complete # shift+tab
    bindkey -M menuselect '\e' send-break
    bindkey -M menuselect '^[[Z' reverse-menu-complete
    bindkey '^[[A' history-beginning-search-backward
    bindkey '^[[B' history-beginning-search-forward

    # alt + e to edit command line in visual
    autoload edit-command-line
    zle -N edit-command-line
    bindkey '^[e' edit-command-line

    # terminal title
    __zsh_update_terminal_title() { echo -en "\033];zsh $PWD\007"; }
    precmd_functions+=(__zsh_update_terminal_title)

    # ls on cd
    __list_all() { ls -a --color=auto >&2; }
    chpwd_functions+=(__list_all)

    if command -v direnv >/dev/null; then eval "$(direnv hook zsh)"; fi

    source_() { if [[ -f $1 ]]; then source "$1"; fi; }
    source_ "${XDG_CONFIG_HOME:-$HOME/.config}"/aliases.sh
    source_ ~/Sync/aliases.sh

    # beam caret
    printf "\x1b[\x36 q"

    if command -v starship &>/dev/null; then
        source <(starship init zsh)
    else
        autoload -Uz promptinit && promptinit && prompt suse
    fi
}

main "$@"
