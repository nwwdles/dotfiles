syntax enable
let $LANG='en'
set nu
set relativenumber
set langmenu=en
set backspace=eol,start,indent
set whichwrap+=<,>,h,l
set ignorecase
set smartcase
set autoread
set hlsearch
set incsearch
set lazyredraw
set magic
set showmatch
set encoding=utf8
set ffs=unix,dos,mac
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set ai "Auto indent
set si "Smart indent
set so=7
set history=500

map <SPACE> <leader>
map <leader>fs :w<cr>
map <leader>qq :q<cr>
map <leader>tw :set wrap!<cr>
map <leader>fS :w !sudo tee %<cr>
map <silent> <leader><cr> :noh<cr>
map <leader>bd :Bclose<cr>:tabclose<cr>gT
map <leader>ba :bufdo bd<cr>
map <leader>l :bnext<cr>
map <leader>h :bprevious<cr>
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove
map <leader>tt :tabnext<cr>
map <leader>x :e ~/buffer<cr>
map <leader>X :e ~/buffer.md<cr>
map <leader>pp :setlocal paste!<cr>
map <leader>P :Plug 'junegunn/vim-easy-align'<cr>
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z
