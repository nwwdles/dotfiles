#!/bin/sh
set -eu

export_cookies() {
    echo "# Netscape HTTP Cookie File"
    sqlite3 -separator "$(printf '\t')" "$1" <<-EOF
	.mode tabs
	.header off
	select host,
	case substr(host,1,1)='.' when 0 then 'FALSE' else 'TRUE' end,
	path,
	case isSecure when 0 then 'FALSE' else 'TRUE' end,
	expiry,
	name,
	value
	from moz_cookies;
EOF
}

locateprofile() {
    profile=$(awk -v RS="(^|\n)\\\[[a-zA-Z0-9]*\\\](\n|$)" '/\nDefault=1/' "$1/profiles.ini" | sed -n 's/^Path=\(.*\)$/\1/p')
    if [ -n "$profile" ]; then
        echo "$1/$profile"
    fi
}

help() {
    echo >&2 "usage: ${0##*/} [-h|--help|-a|PATH]

    Make cookies.txt from cookies.sqlite file."
}

export_from_path() {
    if [ ! -f "$1" ]; then
        echo >&2 "$1 doesn't exist.
The argument must be a path to an existing 'cookies.sqlite' file."
        return 1
    fi

    echo "Using $1" >&2

    # we have to copy the db because it's probably locked
    TMPDB=$(mktemp -u)
    cp "$1" "$TMPDB"
    export_cookies "$TMPDB"
    rm -f "$TMPDB"

}

main() {
    case "${1:-}" in
    --auto)
        profile_dir=$(locateprofile ~/.mozilla/firefox)
        if [ ! -e "$profile_dir" ]; then
            echo >&2 "Failed to locate profile" && return 1
        fi
        export_from_path "$profile_dir/cookies.sqlite"
        ;;
    '' | -h | --help)
        help
        return 1
        ;;
    *)
        export_from_path "$1"
        ;;
    esac
}

main "$@"
