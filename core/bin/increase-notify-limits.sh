#!/bin/sh
# https://docs.syncthing.net/users/faq.html#inotify-limits

f=/etc/sysctl.d/90-override.conf
sudo grep fs.inotify.max_user_watches "$f" \
    || echo "fs.inotify.max_user_watches=204800" | sudo tee -a "$f"
sudo sh -c 'echo 204800 > /proc/sys/fs/inotify/max_user_watches'
