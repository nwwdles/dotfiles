#!/bin/sh
#
# patch vscode to load custom css
#
# the script optionally accepts a list of paths to vscode resource directories
# (they should contain product.json). only the first valid path is patched.

set -eu

# URI to load CSS from
CSS_URL=file://${XDG_CONFIG_HOME:-$HOME/.config}/Code/User/vscode.css

main() {
    # check deps
    for app in jq openssl; do
        if ! command -v "$app" >/dev/null; then
            echo "This script requires '$app' to be installed." >&2
            return 1
        fi
    done

    # find vscode resources dir
    for dir in "$@" \
        /usr/lib/code \
        /usr/share/code/resources/app \
        /usr/share/vscodium-bin/resources/app \
        /opt/vscodium-bin/resources/app \
        /Applications/VSCodium.app/Contents/Resources/app; do
        if [ -f "$dir/product.json" ]; then
            CODE_DIR=$dir
            break
        fi
    done

    if [ ! -d "${CODE_DIR:-}" ]; then
        echo "No VSCode dir found." >&2
        return 1
    fi

    # prepare patch
    PRODUCT_JSON_PATH=$CODE_DIR/product.json
    HTML_SHORT_PATH=vs/code/electron-browser/workbench/workbench.html
    HTML_FULL_PATH=$CODE_DIR/out/$HTML_SHORT_PATH
    PATCH_CONTENT=$(
        cat <<PATCH_EOF
diff --git a/workbench.html b/workbench.html
index 693082b..dc8634b 100644
--- $HTML_FULL_PATH
+++ $HTML_FULL_PATH
@@ -4,6 +4,7 @@
 	<head>
 		<meta charset="utf-8" />
 		<meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src 'self' https: data: blob: vscode-remote-resource:; media-src 'none'; frame-src 'self' vscode-webview: https://*.vscode-webview-test.com; object-src 'self'; script-src 'self' 'unsafe-eval'; style-src 'self' 'unsafe-inline'; connect-src 'self' https: ws:; font-src 'self' https: vscode-remote-resource:;">
 		<meta http-equiv="Content-Security-Policy" content="require-trusted-types-for 'script'; trusted-types default TrustedFunctionWorkaround ExtensionScripts amdLoader cellRendererEditorText defaultWorkerFactory diffEditorWidget domLineBreaksComputer editorViewLayer diffReview extensionHostWorker insane notebookOutputRenderer safeInnerHtml standaloneColorizer tokenizeToString webNestedWorkerExtensionHost webWorkerExtensionHost;">
+		<link rel="stylesheet" type="text/css" href="$CSS_URL"/>
 	</head>
 	<body aria-label="">
 	</body>
PATCH_EOF
    )

    # exit if patch is already applied (prevents sudo prompt on noop runs)
    if ! echo "$PATCH_CONTENT" | patch -d/ -p0 -t -N -r - --dry-run -s >/dev/null 2>&1; then
        echo "CSS patch doesn't apply or it is already applied. Skipping." >&2
        return
    fi

    # apply patch (to undo, just remove `-N` and comment out the check above)
    echo "$PATCH_CONTENT" | sudo patch -d/ -p0 -t -N -r -

    # and update hash so vscode doesn't complain
    new_html_hash=$(openssl dgst -md5 -binary "$HTML_FULL_PATH" | openssl enc -base64 | tr -d '=')
    new_product_json=$(jq 'setpath(["checksums", "'"$HTML_SHORT_PATH"'"]; "'"$new_html_hash"'")' "$PRODUCT_JSON_PATH")
    echo "$new_product_json" | sudo tee "$PRODUCT_JSON_PATH" >/dev/null

    echo "VSCode will now load CSS from: $CSS_URL" >&2
}

main "$@"
