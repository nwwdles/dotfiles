#!/bin/sh
set -eux

gs() { gsettings set "$@"; }

# misc
gs org.gnome.desktop.privacy disable-camera true
gs org.gnome.desktop.privacy disable-microphone true
gs org.gnome.desktop.privacy remember-recent-files false

# wm opts
gs org.gnome.desktop.wm.preferences auto-raise true
gs org.gnome.desktop.wm.preferences auto-raise-delay 300
gs org.gnome.desktop.wm.preferences button-layout "':minimize,maximize,close'"
gs org.gnome.desktop.wm.preferences focus-mode "'sloppy'"
gs org.gnome.desktop.wm.preferences action-right-click-titlebar "'menu'"
gs org.gnome.desktop.wm.preferences resize-with-right-button true

gs org.gnome.desktop.sound allow-volume-above-100-percent true

# keyboard
gs org.gnome.desktop.peripherals.keyboard numlock-state true
gs org.gnome.desktop.peripherals.keyboard remember-numlock-state true
gs org.gnome.desktop.peripherals.keyboard repeat-interval 13
gs org.gnome.desktop.peripherals.keyboard delay 180
gs org.gnome.desktop.input-sources sources "[('xkb', 'us'), ('xkb', 'ru')]"

# gs org.gnome.desktop.input-sources xkb-options "['lv3:ralt_switch', 'compose:sclk', 'ctrl:swapcaps', 'terminate:ctrl_alt_bksp', 'grp:win_space_toggle', 'grp:toggle']"
gs org.gnome.desktop.input-sources xkb-options "['lv3:ralt_switch', 'compose:sclk', 'terminate:ctrl_alt_bksp', 'grp:win_space_toggle', 'grp:toggle']"
gs org.gnome.shell.keybindings switch-to-application-1 []
gs org.gnome.shell.keybindings switch-to-application-2 []
gs org.gnome.shell.keybindings switch-to-application-3 []
gs org.gnome.shell.keybindings switch-to-application-4 []
gs org.gnome.shell.keybindings switch-to-application-5 []
gs org.gnome.shell.keybindings switch-to-application-6 []
gs org.gnome.shell.keybindings switch-to-application-7 []
gs org.gnome.shell.keybindings switch-to-application-8 []
gs org.gnome.shell.keybindings switch-to-application-9 []
gs org.gnome.desktop.wm.keybindings move-to-monitor-down []
gs org.gnome.desktop.wm.keybindings move-to-monitor-left []
gs org.gnome.desktop.wm.keybindings move-to-monitor-right []
gs org.gnome.desktop.wm.keybindings move-to-monitor-up []
gs org.gnome.desktop.wm.keybindings toggle-on-all-workspaces []
gs org.gnome.desktop.wm.keybindings switch-to-workspace-left []
gs org.gnome.desktop.wm.keybindings switch-to-workspace-right []
gs org.gnome.desktop.wm.keybindings move-to-workspace-left []
gs org.gnome.desktop.wm.keybindings move-to-workspace-right []
gs org.gnome.desktop.wm.keybindings switch-to-workspace-last []
gs org.gnome.desktop.wm.keybindings move-to-workspace-last []
gs org.gnome.desktop.wm.keybindings unmaximize []
gs org.gnome.desktop.wm.keybindings maximize []

gs org.gnome.desktop.wm.keybindings show-desktop "['<Super>D']"
gs org.gnome.desktop.wm.keybindings minimize "['<Super>M']"
gs org.gnome.desktop.wm.keybindings toggle-maximized "['<Super>Z']"
gs org.gnome.desktop.wm.keybindings toggle-fullscreen "['<Super><Ctrl>F']"
gs org.gnome.desktop.wm.keybindings close "['<Super>q']"

gs org.gnome.desktop.wm.keybindings move-to-workspace-1 "['<Super><Shift>1']"
gs org.gnome.desktop.wm.keybindings move-to-workspace-2 "['<Super><Shift>2']"
gs org.gnome.desktop.wm.keybindings move-to-workspace-3 "['<Super><Shift>3']"
gs org.gnome.desktop.wm.keybindings move-to-workspace-4 "['<Super><Shift>4']"
gs org.gnome.desktop.wm.keybindings move-to-workspace-5 "['<Super><Shift>5']"
gs org.gnome.desktop.wm.keybindings move-to-workspace-6 "['<Super><Shift>6']"
gs org.gnome.desktop.wm.keybindings move-to-workspace-7 "['<Super><Shift>7']"
gs org.gnome.desktop.wm.keybindings move-to-workspace-8 "['<Super><Shift>8']"
gs org.gnome.desktop.wm.keybindings move-to-workspace-9 "['<Super><Shift>9']"

gs org.gnome.desktop.wm.keybindings switch-to-workspace-1 "['<Super>1']"
gs org.gnome.desktop.wm.keybindings switch-to-workspace-2 "['<Super>2']"
gs org.gnome.desktop.wm.keybindings switch-to-workspace-3 "['<Super>3']"
gs org.gnome.desktop.wm.keybindings switch-to-workspace-4 "['<Super>4']"
gs org.gnome.desktop.wm.keybindings switch-to-workspace-5 "['<Super>5']"
gs org.gnome.desktop.wm.keybindings switch-to-workspace-6 "['<Super>6']"
gs org.gnome.desktop.wm.keybindings switch-to-workspace-7 "['<Super>7']"
gs org.gnome.desktop.wm.keybindings switch-to-workspace-8 "['<Super>8']"
gs org.gnome.desktop.wm.keybindings switch-to-workspace-9 "['<Super>9']"

gs org.gnome.desktop.wm.keybindings move-to-workspace-down "['<Super><Shift>PgDown']"
gs org.gnome.desktop.wm.keybindings move-to-workspace-up "['<Super><Shift>PgUp']"
gs org.gnome.desktop.wm.keybindings switch-to-workspace-down "['<Super>PgDown']"
gs org.gnome.desktop.wm.keybindings switch-to-workspace-up "['<Super>PgUp']"

gs org.gnome.mutter.keybindings toggle-tiled-right "['<Super><Alt>Right', '<Super><Alt>L']"
gs org.gnome.mutter.keybindings toggle-tiled-left "['<Super><Alt>Left', '<Super><Alt>H']"

# gs org.gnome.shell.extensions.org-lab21-putwindow move-focus-east "['<Super>Right']"
# gs org.gnome.shell.extensions.org-lab21-putwindow move-focus-north "['<Super>Up']"
# gs org.gnome.shell.extensions.org-lab21-putwindow move-focus-south "['<Super>Down']"
# gs org.gnome.shell.extensions.org-lab21-putwindow move-focus-west "['<Super>Left']"

gs org.gnome.GPaste images-support true
gs org.gnome.GPaste show-history '<Super>C'
gs org.gnome.GPaste empty-history-confirmation false

gs org.gnome.gnome-screenshot auto-save-directory "file:///$HOME/Pictures/Screenshots"
