#!/bin/sh
# https://unix.stackexchange.com/questions/382060/change-default-sudo-password-timeout
f=/etc/sudoers.d/"$USER"
sudo grep timestamp_timeout "$f" \
    || echo 'Defaults timestamp_timeout=60' | sudo tee -a "$f"
