#!/bin/sh
#
# https://github.com/waldner/Firefox-RunWith
# https://addons.mozilla.org/en-US/firefox/addon/run-with/

manifest_dest="$HOME/.mozilla/native-messaging-hosts/runwith.json"
executable_dest="$HOME/.local/bin/runwith.py"
mkdir -p "$(dirname "$manifest_dest")"
mkdir -p "$(dirname "$executable_dest")"
curl -s "https://raw.githubusercontent.com/waldner/Firefox-RunWith/master/runwith.json" >"$manifest_dest"
curl -s "https://raw.githubusercontent.com/waldner/Firefox-RunWith/master/runwith.py" >"$executable_dest"
chmod +x "$executable_dest"
sed -s "s|/path/to/runwith.py|$executable_dest|" -i "$manifest_dest"
echo "Done!" >&2
