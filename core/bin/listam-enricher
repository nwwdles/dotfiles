#!/usr/bin/env bash
set -euo pipefail

INFILE=all
OUTFILE=enriched

usage() { echo "usage: ${0##*/} [-h][-o outfile][-i inputfile]"; }
while getopts "hi:o:" o; do
    case "${o}" in
    o) OUTFILE=$o ;;
    i) INFILE=$o ;;
    *)
        usage
        exit
        ;;
    esac
done

fetch_userinfo() {
    curl -s "https://www.list.am/?w=12&&i=$1" -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0' -H 'Accept: text/html, */*; q=0.01' -H 'Accept-Language: en,en-US;q=0.8,ru-RU;q=0.5,ru;q=0.3' -H 'Accept-Encoding: gzip' -H 'X-Requested-With: XMLHttpRequest' -H 'Connection: keep-alive' -H "Referer: https://www.list.am/item/$1" -H 'Cookie: lang=1; __stripe_mid=039dc690-0096-41ce-8d8b-37ac62c16963999e2f; __stripe_sid=36440473-b6ac-4b58-a0e4-f34fd1b1eabdb098d0' -H 'Sec-Fetch-Dest: empty' -H 'Sec-Fetch-Mode: cors' -H 'Sec-Fetch-Site: same-origin' -H 'TE: trailers' | gunzip
}

extractphones() {
    htmlq '.phones > a' -a href | paste -sd ',' | tr -d '\n'
}

extractrating() {
    htmlq '.r > script' -t | jq '.ratingValue'
}

extractnumratings() {
    htmlq '.r > script' -t | jq -r '.ratingCount'
}

extractname() {
    htmlq '.n > div' -t
}

extract_registered_at() {
    htmlq '.since' --text
}

extractuserurl() {
    htmlq '.n' -a href | sed 's|^|https://www.list.am|g'
}

fetch_iteminfo() {
    curl -s "https://www.list.am/item/$1" -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: en,en-US;q=0.8,ru-RU;q=0.5,ru;q=0.3' -H 'Accept-Encoding: gzip' -H 'Connection: keep-alive' -H 'Cookie: lang=1; __stripe_mid=039dc690-0096-41ce-8d8b-37ac62c16963999e2f; __stripe_sid=af7f2f30-2fda-4bdb-8c0f-ad122f2e91c550afa7' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: none' -H 'Sec-Fetch-User: ?1' | gunzip
}

extract_updated_at() {
    htmlq '.footer>span' -t | grep 'Обновлено' | sed 's/\w*: //'
}

enrich() {
    while read -r item_id item_url rest; do
        r=$(fetch_userinfo "$item_id")
        phones=$(echo "$r" | extractphones)
        rating=$(echo "$r" | extractrating)
        numratings=$(echo "$r" | extractnumratings)
        registered_at=$(echo "$r" | extract_registered_at)
        name=$(echo "$r" | extractname)
        userurl=$(echo "$r" | extractuserurl)

        updated_at=$(fetch_iteminfo "$item_id" | extract_updated_at)

        printf '%s\t' "$item_id" "$item_url" "$userurl" "$name" "$rating" "$numratings" "$registered_at" "$phones" "$rest" "$updated_at"
        printf '\n'
    done
}

get_new() {
    grep -Fvf <(cut -f1 "$OUTFILE") "$INFILE"
}

get_deleted() {
    grep -Fvf <(cut -f1 "$INFILE") "$OUTFILE"
}

main() {
    while true; do
        was=$(cat "$OUTFILE") || true

        new=$(get_new) || true
        if [ -n "$new" ]; then
            x=$(echo "$new" | head -n10 | pv -l -L 3 | enrich)
            echo "$x" >>"$OUTFILE"
        fi

        deleted=$(get_deleted) || true
        if [ -n "$deleted" ]; then
            x=$(grep -Ff <(cut -f1 "$INFILE") "$OUTFILE") || true
            if [ -n "$x" ]; then
                echo "$x" >"$OUTFILE"
            fi
        fi

        d=$(diff <(echo "$was") "$OUTFILE" | grep -E '^>' || true)
        if [ -n "$d" ]; then
            date
            echo "$d"
            echo
        fi

        sleep 2
    done
}

main "$@"
