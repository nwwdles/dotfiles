#!/bin/sh
# https://askubuntu.com/questions/387757/how-to-make-password-show-asterisks-as-i-type-it-when-running-sudo-command
f=/etc/sudoers.d/"$USER"
sudo grep pwfeedback "$f" \
    || echo 'Defaults pwfeedback' | sudo tee -a "$f"
