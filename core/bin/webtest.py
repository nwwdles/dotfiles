#!/usr/bin/env python3
from os import getenv
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

home = getenv("HOME")

options = webdriver.ChromeOptions()
options.add_argument(f"--user-data-dir={home}/.config/chromium")
options.add_argument("--profile-directory=Profile 1")

driver = webdriver.Chrome("chromedriver", options=options)
driver.delete_all_cookies()
driver.get("https://staging-www.k.avito.ru")

elem = driver.find_element(By.NAME, "login")
elem.send_keys("abvc")
