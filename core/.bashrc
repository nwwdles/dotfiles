#
# ~/.bashrc
#
# shellcheck shell=bash

# only run rc if interactive
[[ $- != *i* ]] && return

xhost +local:root >/dev/null 2>&1

HISTCONTROL=ignoreboth:erasedups
HISTFILESIZE=
HISTSIZE=
if [[ -e "$HOME/Sync/.bash_history" ]]; then
    HISTFILE="$HOME/Sync/.bash_history"
fi

bind "set completion-ignore-case on"
bind "set show-all-if-ambiguous on"
bind "TAB: menu-complete"
bind '"\e[Z": menu-complete-backward'
bind '"\002": complete'
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

complete -cf sudo

shopt -s checkwinsize # Bash won't get SIGWINCH if another process is in the foreground. Make it check when it regains control.
shopt -s autocd       # cd by folder name (can type .. instead of cd ..)
shopt -s expand_aliases
shopt -s histappend # Enable history appending instead of overwriting.

cd() { builtin cd "$1" && ls >&2; }

__source() { [[ -f $1 ]] && source "$1"; }

__source "${XDG_CONFIG_HOME:-$HOME/.config}/aliases.sh"
__source '/usr/share/bash-completion/bash_completion'
__source '/usr/share/doc/pkgfile/command-not-found.bash'
__source '/usr/share/fzf/completion.bash'
__source '/usr/share/fzf/key-bindings.bash'
if command -v fzf-share &>/dev/null; then
    __source "$(fzf-share)/completion.bash"
    __source "$(fzf-share)/key-bindings.bash"
fi

if command -v starship &>/dev/null; then
    eval "$(starship init bash)"
fi
