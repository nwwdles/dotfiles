#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/shape.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

volatile sig_atomic_t done = 0;

void term(int signum) { done = 1; }

static void x_set_wm(Display *d, Window w)
{
    char *wname = "abc";

    XStoreName(d, w, wname);
    Atom nameAtom = XInternAtom(d, "_NET_WM_NAME", 0);
    XChangeProperty(d, w, nameAtom, XInternAtom(d, "UTF8_STRING", 0), 8,
                    PropModeReplace, (unsigned char *)wname, 4);

    XClassHint classhint = {wname, wname};
    XSetClassHint(d, w, &classhint);

    Atom typeAtom =
        XInternAtom(d, "_NET_WM_WINDOW_TYPE", 0); // Let WM know type.
    Atom property[2]; // Change 2 things at once, (parent + 2 children).
    property[0] = XInternAtom(d, "_NET_WM_WINDOW_TYPE_NOTIFICATION", 0);
    // property[1] = XInternAtom(d, "_NET_WM_WINDOW_TYPE_DIALOG", 0);
    XChangeProperty(d, w, typeAtom, XA_ATOM, 32, PropModeReplace,
                    (unsigned char *)property, 1L);

    // Let WM know state.
    Atom stateAtom = XInternAtom(d, "_NET_WM_STATE", 0);
    property[0] = XInternAtom(d, "_NET_WM_STATE_MODAL", 0);
    XChangeProperty(d, w, stateAtom, XA_ATOM, 32, PropModeReplace,
                    (unsigned char *)property, 1L);
}

int draw_option(Display *d, Window w, GC g, int x, int y, int width, int height,
                int selected, char *msg)
{
    int s = DefaultScreen(d);
    XSetForeground(d, g, BlackPixel(d, s));
    if (selected) {
        XFillRectangle(d, w, g, x, y, width, height);
        XSetForeground(d, g, WhitePixel(d, s));
    } else {
        // XDrawRectangle(d, w, g, x, y, width, height);
    }
    XDrawString(d, w, g, x + 4, y + height / 2, msg, strlen(msg));
    return 0;
}

int read_stdin(char ***ret, char delim)
{
    int currentMax = 16;
    char **lines = (char **)malloc(sizeof(char *) * currentMax);
    int line = 0;
    size_t lim = 1024;
    while (getdelim(lines + line, &lim, delim, stdin) > 0) {
        char *s = lines[line];
        if (s[strlen(s) - 1] == delim) {
            s[strlen(s) - 1] = '\0';
        }
        line++;
        if (line >= currentMax) {
            currentMax *= 2;
            lines = (char **)realloc(lines, sizeof(char *) * currentMax);
        }
    }
    *ret = lines;
    return line;
}

int warp_window_to_cursor(Display *d, Window w, int ww, int hh)
{
    XWindowAttributes xwa;
    XGetWindowAttributes(d, w, &xwa);

    Window window_returned;
    int root_x, root_y, win_x, win_y;
    unsigned int mask_return;
    XQueryPointer(d, w, &window_returned, &window_returned, &root_x, &root_y,
                  &win_x, &win_y, &mask_return);
    int dx = win_x - ww / 2;
    int dy = win_y - hh / 2;

    // if (win_x >= ww) {
    //     dx = win_x - (ww - 50);
    // } else if (win_x >= 0) {
    //     dx = 0;
    // } else {
    //     dx = win_x - 50;
    // }
    // if (win_y > hh) {
    //     dy = win_y - (hh - 50);
    // } else if (win_y >= 0) {
    //     dy = 0;
    // } else {
    //     dy = win_y - 50;
    // }
    return XMoveWindow(d, w, xwa.x + dx, xwa.y + dy);
}

int main(void)
{
    char **lines;
    int numlines = read_stdin(&lines, '\n');

    signal(SIGTERM, term);

    Display *d;
    Window w;
    int ww = 300, hh = 300;
    d = XOpenDisplay(NULL);
    if (d == NULL) {
        fprintf(stderr, "Cannot open display\n");
        exit(1);
    }
    int s = DefaultScreen(d);

    Window window_returned;
    int root_x, root_y, win_x, win_y;
    unsigned int mask_return;
    XQueryPointer(d, RootWindow(d, s), &window_returned, &window_returned,
                  &root_x, &root_y, &win_x, &win_y, &mask_return);
    w = XCreateSimpleWindow(d, RootWindow(d, s), root_x - ww / 2,
                            root_y - hh / 2, ww, hh, 1, BlackPixel(d, s),
                            WhitePixel(d, s));
    x_set_wm(d, w);
    XSelectInput(d, w,
                 ExposureMask | ButtonPressMask | ButtonReleaseMask |
                     LeaveWindowMask | PointerMotionMask | FocusChangeMask);
    XMapWindow(d, w);

    int sidesize;
    sidesize = (int)(ceil(sqrt(numlines)));
    int items_x = sidesize;
    int items_y = sidesize;
    int itempos_x = -1;
    int itempos_y = -1;
    int selected_line = -1;
    XEvent e;
    while (XNextEvent(d, &e) == 0 && !done) {
        printf("%d\n", e.type);

        switch (e.type) {
        case Expose:
            warp_window_to_cursor(d, w, ww, hh);
        case MotionNotify: {
            XQueryPointer(d, w, &window_returned, &window_returned, &root_x,
                          &root_y, &win_x, &win_y, &mask_return);
            int xx = (win_x / (ww / items_x));
            int yy = (win_y / (hh / items_y));
            if (xx > items_x - 1) {
                xx = items_x - 1;
            }
            if (yy > items_y - 1) {
                xx = items_y - 1;
            }
            if (xx == itempos_x && yy == itempos_y) {
                continue;
            }
            // printf("%d %d\n", xx, yy);
            itempos_x = xx;
            itempos_y = yy;

            XClearWindow(d, w);
            for (int i = 0; i < numlines; i++) {
                int ix = i % items_x;
                int iy = i / items_x;
                int selected = 0;
                if (ix == itempos_x && iy == itempos_y) {
                    selected_line = i;
                    selected = 1;
                }
                draw_option(d, w, DefaultGC(d, s), ix * (ww / items_x),
                            iy * (hh / items_y), ww / items_x + 2,
                            hh / items_y + 1, selected, lines[i]);
            }
            continue;
        }
        case FocusOut:
        case LeaveNotify:
            warp_window_to_cursor(d, w, ww, hh);
            continue;
        case ButtonPress:
        case ButtonRelease:
            break;
        default:
            continue;
        }
        // XQueryPointer(d, w, &window_returned, &window_returned, &root_x,
        //               &root_y, &win_x, &win_y, &mask_return);
        // printf("%d\t%d\n", win_x, win_y);
        break;
    }
    printf("%s\n", lines[selected_line]);

    XCloseDisplay(d);
    return 0;
}
