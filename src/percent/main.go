package main

import (
	"bufio"
	"fmt"
	"os"
	"text/tabwriter"
)

type line struct {
	count int
	text  string
}

func main() {
	r := bufio.NewScanner(os.Stdin)

	lines := make([]line, 0, 100)
	for r.Scan() {
		l := line{}
		fmt.Sscanf(r.Text(), "%d %s", &l.count, &l.text)
		lines = append(lines, l)
	}
	total := 0
	for _, l := range lines {
		total += l.count
	}

	w := tabwriter.NewWriter(os.Stdout, 5, 1, 1, ' ', 0)
	for _, l := range lines {
		fmt.Fprintf(w, "%v\t(%.2f%%)\t%s\n", l.count, float64(l.count)/float64(total)*100, l.text)
	}
	w.Flush()
}
