package main

import (
	"flag"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"
)

type file struct {
	fname string
	fs.FileInfo
}

func main() {
	flag.Parse()
	path := flag.Arg(0)
	d, err := os.ReadDir(path)
	if err != nil {
		log.Fatalln(err)
	}

	var files []file
	for _, entry := range d {
		if entry.IsDir() {
			continue
		}
		switch strings.ToLower(filepath.Ext(entry.Name())) {
		default:
			continue
		case ".mp4", ".webm", ".png", ".jpg", ".jpeg", ".gif":
		}

		info, err := entry.Info()
		if err != nil {
			log.Fatalln(err)
		}

		files = append(files, file{filepath.Join(path, info.Name()), info})
	}

	sort.Slice(files, func(i, j int) bool { return files[i].ModTime().Before(files[j].ModTime()) })
	grouped := [][]file{}

	prevTime := files[0].ModTime().Round(30 * time.Minute)
	var buf []file
	for _, f := range files {
		t := f.ModTime().Round(30 * time.Minute)
		if t != prevTime {
			grouped = append(grouped, buf)
			buf = []file{}
		}
		prevTime = t
		buf = append(buf, f)
	}
	if len(buf) > 0 {
		grouped = append(grouped, buf)
	}

	compact := [][]file{}
	buf = nil
	for _, v := range grouped {
		if len(v) >= 5 && len(buf) > 0 {
			compact = append(compact, buf)
			buf = []file{}
		}
		buf = append(buf, v...)
	}
	if len(buf) > 0 {
		compact = append(compact, buf)
	}

	for _, v := range compact {
		t := v[0].ModTime().Format("20060102-150405")

		fmt.Printf("%v\t%v\n", t, len(v))
		for _, f := range v {
			old := f.fname
			newdir := filepath.Join(filepath.Dir(old), "web", t)
			new := filepath.Join(newdir, filepath.Base(old))
			if _, err := os.Stat(new); err == nil {
				log.Println("skipping because of existing file:", new)
				continue
			}
			if err := os.MkdirAll(newdir, 0o744); err != nil {
				log.Fatalln(err, newdir)
			}
			if err := os.Rename(old, new); err != nil {
				log.Fatalln(err, old, new)
			}
		}
	}
}
