package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"strings"
)

func main() {
	type x struct {
		URL   *url.URL
		Query url.Values
	}
	b := bufio.NewScanner(os.Stdin)
	for b.Scan() {
		s := b.Text()
		if strings.TrimSpace(s) == "" {
			continue
		}
		u, _ := url.Parse(s)
		q, _ := url.ParseQuery(u.RawQuery)
		e := json.NewEncoder(os.Stdout)
		e.SetIndent("", "  ")
		e.Encode(x{
			URL:   u,
			Query: q,
		})

		for k, v := range q {
			for _, v := range v {
				fmt.Printf("%v %v\n", k, v)
			}
		}
	}
}
