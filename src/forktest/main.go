package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"syscall"
	"time"
)

func run() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGHUP, syscall.SIGTERM, syscall.SIGINT, syscall.SIGQUIT)
	i := 0
	for {
		select {
		case sig := <-c:
			log.Println("child:", sig)
			os.Exit(0)
		case <-time.After(100 * time.Millisecond):
			fmt.Println("hello", i)
			i++
			if i > 20 {
				panic("oh no")
			}
		}
	}
}

type Logger struct{}

func (w Logger) Write(b []byte) (int, error) {
	fmt.Printf("[%s]\n", b)
	return len(b), nil
}

func execute() bool {
	if len(os.Args) > 1 {
		return false
	}
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGHUP, syscall.SIGTERM, syscall.SIGINT, syscall.SIGQUIT)
	go func() {
		log.Println("parent:", <-c)
	}()

	cmd := exec.Command(os.Args[0], "-")

	cmd.Stdin = os.Stdin
	cmd.Stderr = io.MultiWriter(os.Stderr, Logger{})
	cmd.Stdout = io.MultiWriter(os.Stdout, Logger{})
	fmt.Println("starting new process...")
	err := cmd.Run()
	if err != nil {
		fmt.Println("process returned error:", err)
		os.Exit(1)
	}
	return true
}

func main() {
	var i int
	log.Println(i == *new(int))
	// if execute() {
	// 	return
	// }
	// run()
}
