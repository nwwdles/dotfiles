// package main
// func main(){d:="         12345678987654321"
// for i:=range d[9:]{if i>8{i=16-i}
// println(d[i:10+i]+d[26-i:])}}

// package main
// func main(){d:=`        12345678987654321
// `
// for i:=range d[9:]{if i>8{i=16-i}
// print(d[i:9+i]+d[25-i:])}}

// package main
// func main(){d:=`        12345678987654321`
// for _,j:=range d[8:]{println(d[j-49:j-40]+d[74-j:])}}

// % jq -rn '"12345678987654321"as$s|$s|explode[]|.-48|(9-.)*" "+$s[:.-1]+$s[-.:]'
//

package main
func main(){i,t:=-9,0
for i<8{k:=i>>4
t=(t-99*k*t)/10+k
i++
println(i,k,t,`        `[:k+i^k],t*t)}}
