package main

import "testing"

func Benchmark_inferTemplate(b *testing.B) {
	var ss []string
	for i := 0; i < 1000; i++ {
		ss = append(ss, string(rune(int('a')+i%4)))
	}
	b.Run("slice", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			for _, v := range ss {
				for _, s := range []string{"a", "b", "c"} {
					_ = s == v
				}
			}
		}
	})
	b.Run("array", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			for _, v := range ss {
				for _, s := range [...]string{"a", "b", "c"} {
					_ = s == v
				}
			}
		}
	})
	b.Run("slice2", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			for _, s := range []string{"a", "b", "c"} {
				for _, v := range ss {
					_ = s == v
				}
			}
		}
	})
	b.Run("array2", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			for _, s := range [...]string{"a", "b", "c"} {
				for _, v := range ss {
					_ = s == v
				}
			}
		}
	})
}
