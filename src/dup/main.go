package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

var (
	size = flag.Int("s", 64, "")
	n    = flag.Int("n", 4, "")
)

func main() {
	flag.Parse()
	r := bufio.NewReader(os.Stdin)
	var buf []byte
	i := 0
	for {
		s, err := r.ReadString('\n')
		if err != nil {
			for i := 0; i < *n; i++ {
				fmt.Print(string(buf))
			}
			return
		}
		buf = append(buf, s...)
		i++
		if i >= *size {
			i -= *size
			for i := 0; i < *n; i++ {
				fmt.Print(string(buf))
			}
			buf = buf[:0]
		}
	}
}
