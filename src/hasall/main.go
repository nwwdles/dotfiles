package main

import (
	"bufio"
	"errors"
	"flag"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	flag.Parse()

	f1, err := os.Open(flag.Args()[0])
	if err != nil {
		log.Fatal(err)
	}
	f2, err := os.Open(flag.Args()[1])
	if err != nil {
		log.Fatal(err)
	}

	r := bufio.NewReader(f1)

	m := make(map[string]struct{})
	for {
		l, _, err := r.ReadLine()
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
			log.Fatal(err)
		}
		m[strings.TrimSpace(string(l))] = struct{}{}
	}

	r2 := bufio.NewReader(f2)
	for {
		l, _, err := r2.ReadLine()
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
			log.Fatal(err)
		}
		delete(m, string(l))
	}

	for k := range m {
		log.Fatal("line ", string(k), " doesn't exist in ", flag.Args()[1])
	}
}
