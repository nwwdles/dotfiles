package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var (
	delim = flag.String("d", "\x00", "entry delimiter")
	list  = flag.Bool("l", false, "list all elems and their counts")
)

var synonyms = map[string][]string{
	"ame":      {"watson_amelia"},
	"gura":     {"gawr_gura"},
	"gurame":   {"gawr_gura", "amelia_watson"},
	"takamori": {"takanashi_kiara", "mori_calliope"},
	"iname":    {"watson_amelia", "ninomae_ina'nis"},
	"ina":      {"ninomae_ina'nis"},
	"mori":     {"mori_calliope"},
	"kiara":    {"takanashi_kiara"},
	"maho":     {"hiyajou_maho"},
	"kurisu":   {"makise_kurisu"},
	"okakoro":  {"inugami_korone", "nekomata_okayu"},
	"korone":   {"inugami_korone"},
	"okayu":    {"nekomata_okayu"},
}

func walk(path string, f func(abspath string, elems map[string]struct{})) error {
	return filepath.Walk(flag.Args()[0], func(path string, info os.FileInfo, err error) error {
		switch {
		case err != nil:
			log.Println(err)
			return nil
		case info.IsDir():
			return nil
		}

		abs, err := filepath.Abs(path)
		if err != nil {
			return err
		}

		els := make(map[string]struct{})
		p := strings.NewReplacer(
			string(os.PathSeparator), " ",
			".", " ",
			",", " ",
		).Replace(abs)

		for _, elem := range strings.Split(p, " ") {
			if elem != "" {
				els[strings.ToLower(elem)] = struct{}{}
				for _, v := range synonyms[elem] {
					els[strings.ToLower(v)] = struct{}{}
				}
			}
		}

		f(abs, els)

		return nil
	})
}

func toLowerAll(s ...string) (out []string) {
	for _, st := range s {
		out = append(out, strings.ToLower(st))
	}
	return out
}

func main() {
	flag.Parse()
	if len(flag.Args()) < 1 {
		fmt.Println("provide a path to the directory to search in")
		os.Exit(1)
	}

	var query []string
	var exclude []string

	rawQuery := flag.Args()[1:]
	for _, v := range rawQuery {
		t := &query
		if v[0] == '-' {
			t = &exclude
			v = v[1:]
		}

		if s, ok := synonyms[v]; ok {
			*t = append(*t, toLowerAll(s...)...)
		} else {
			*t = append(*t, strings.ToLower(v))
		}
	}

	switch {
	case *list:
		counts := make(map[string]int)
		err := walk(flag.Args()[0], func(abs string, pe map[string]struct{}) {
			for k := range pe {
				counts[k]++
			}
		})
		if err != nil {
			panic(err)
		}

		for k, v := range counts {
			fmt.Printf("%d %s\n", v, k)
		}
	default:
		err := walk(flag.Args()[0], func(abs string, pe map[string]struct{}) {
			if strings.Contains(abs, *delim) {
				log.Printf("skipping %v due to filename containing delimiter", abs)
				return
			}

			for _, q := range query {
				if _, ok := pe[q]; !ok {
					return
				}
			}
			for _, q := range exclude {
				if _, ok := pe[q]; ok {
					return
				}
			}
			fmt.Printf("%s%s", abs, *delim)
		})
		if err != nil {
			panic(err)
		}
	}
}
