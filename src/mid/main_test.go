package main

import (
	"testing"
)

func GetMiddle(s string) string {
	r := []rune(s)
	l := len(r)
	switch {
	case l <= 1:
		return s
	case l%2 == 0:
		return string(r[l/2-1 : l/2+1])
	default:
		return string(r[l/2 : l/2+2])
	}
}

func Test(t *testing.T) {
	tests := []struct {
		name string
		in   string
		want string
	}{
		{in: "", want: ""},
		{in: "а", want: "а"},
		{in: "аб", want: "аб"},
		{in: "абв", want: "бв"},
		{in: "абвг", want: "бв"},
		{in: "абвгд", want: "вг"},
		{in: "012345678", want: "45"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetMiddle(tt.in); got != tt.want {
				t.Errorf("got %v, want %v\n", got, tt.want)
			}
		})
	}
}
