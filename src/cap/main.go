package main

import "fmt"

func main() {
	a := make([]int, 0, 3)
	for i := 0; i < 100; i++ {
		a = append(a, i)
		fmt.Println("len", len(a), "cap", cap(a))
	}
}
