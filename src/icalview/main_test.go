package main

import (
	"io"
	"reflect"
	"strings"
	"testing"
	"time"
)

func Test_parse(t *testing.T) {
	type args struct{}
	tests := []struct {
		name    string
		r       io.Reader
		want    []Cal
		wantErr bool
	}{
		{
			r: strings.NewReader(`
BEGIN:VCALENDAR
BEGIN:VEVENT
UID:TU992495502
DTSTART:20210722T200000Z
SEQUENCE:0
TRANSP:OPAQUE
DTEND:20210722T210000Z
URL:https://teamup.com/ksua2ar4zft49pdn7c/events/992495502
SUMMARY:Collab Time!
CLASS:PUBLIC
CATEGORIES:Ninomae Ina'nis
DTSTAMP:20220122T052104Z
CREATED:20210829T160507Z
END:VEVENT
END:VCALENDAR
`),
			want: []Cal{{
				Events: []Event{
					{
						Summary:    `Collab Time!`,
						Categories: []string{`Ninomae Ina'nis`},
						URL:        `https://teamup.com/ksua2ar4zft49pdn7c/events/992495502`,
						DateStart: time.Date(2021, 07, 22,
							20, 0, 0, 0, time.UTC),
						DateEnd: time.Date(2021, 07, 22,
							21, 0, 0, 0, time.UTC),
					},
				},
			}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parse(tt.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parse() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parseDatetime(t *testing.T) {
	tests := []struct {
		name     string
		timezone string
		val      string
		want     time.Time
		wantErr  bool
	}{
		{
			timezone: "",
			val:      "20210722T200000Z",
			want: time.Date(2021, 07, 22,
				20, 0, 0, 0, time.UTC),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseDatetime(tt.timezone, tt.val)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseDatetime() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseDatetime() = %v, want %v", got, tt.want)
			}
		})
	}
}
