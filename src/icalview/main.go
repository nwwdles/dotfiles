package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strings"
	"text/tabwriter"
	"time"
)

var debug = flag.Bool("d", false, "")

type Event struct {
	DateStart  time.Time
	DateEnd    time.Time
	Summary    string
	Categories []string
	URL        string
}

type Cal struct {
	Events []Event
}

func d(format string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, format, a...)
}

func parse(r io.Reader) ([]Cal, error) {
	br := bufio.NewReader(r)

	var cal *Cal
	var ev *Event
	var out []Cal
	parseString := func(s string) error {
		col := strings.IndexByte(s, ':')
		if col < 0 {
			return fmt.Errorf("bad line: %v", s)
		}

		key, val := s[:col], s[col+1:]
		val = strings.ReplaceAll(val, `\"`, `"`)

		switch key {
		case "BEGIN":
			switch val {
			case "VEVENT":
				ev = &Event{}
			case "VCALENDAR":
				cal = &Cal{}
			}
		case "END":
			switch val {
			case "VEVENT":
				cal.Events = append(cal.Events, *ev)
				ev = nil
			case "VCALENDAR":
				out = append(out, *cal)
				cal = nil
			}
		case "SUMMARY":
			ev.Summary = val
		case "CATEGORIES":
			ev.Categories = strings.Split(val, ",")
		case "URL":
			ev.URL = val
		default:
			keyparts := strings.Split(key, ";")
			switch keyparts[0] {
			case "DTSTART", "DTEND":
				for _, tz := range keyparts {
					tz = strings.TrimPrefix(tz, "TZID=")
					tz = strings.TrimPrefix(tz, "DTSTART")
					tz = strings.TrimPrefix(tz, "DTEND")
					t, err := parseDatetime(tz, val)
					if err != nil {
						return err
					}

					if ev != nil {
						if keyparts[0] == "DTSTART" {
							ev.DateStart = t
						} else {
							ev.DateEnd = t
						}
					}
				}
			default:
			}
		}
		if *debug {
			d("%v\tev:%v\tcal:%v \n", s, ev, cal)
		}
		return nil
	}

	var prev string
	for {
		s, err := br.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}

		if s[0] == '\t' || s[0] == ' ' {
			prev += strings.TrimRight(s[1:], "\r\n")
			continue
		}

		if prev != "" {
			if err = parseString(prev); err != nil {
				return nil, err
			}
		}

		prev = strings.TrimRight(s, "\r\n")
	}

	if err := parseString(prev); err != nil {
		return nil, err
	}

	return out, nil
}

func parseDatetime(timezone string, val string) (time.Time, error) {
	tz := time.UTC
	if timezone != "" {
		tzName := strings.TrimPrefix(timezone, "TZID=")
		t, err := time.LoadLocation(tzName)
		if err == nil {
			tz = t
		}
	}

	t, err := time.ParseInLocation("20060102T150405Z", val, tz)
	if err != nil {
		t, err = time.ParseInLocation("20060102T150405", val, tz)
		if err != nil {
			t, err = time.ParseInLocation("20060102", val, tz)
			if err != nil {
				return time.Time{}, err
			}
		}
	}
	return t, nil
}

func main() {
	flag.Parse()
	c, err := parse(os.Stdin)
	if err != nil {
		log.Fatalln(err)
	}

	var evs []Event
	for _, cal := range c {
		evs = append(evs, cal.Events...)
	}

	sort.Slice(evs, func(i, j int) bool { return evs[i].DateStart.Before(evs[j].DateStart) })
	w := tabwriter.NewWriter(os.Stdout, 1, 1, 1, ' ', tabwriter.TabIndent)
	defer w.Flush()
	p := false
	for _, ev := range evs {
		if ev.DateStart.After(time.Now().Add(-12 * time.Hour)) {
			if ev.DateStart.After(time.Now()) && !p {
				p = true
				fmt.Fprintf(w, "--- now ---\t \n")
			}
			fmt.Fprintf(w, "%s\t[%s]\t%s\n",
				ev.Summary,
				ev.DateStart.Local().Format("Mon, 02.01 15:04"),
				strings.Join(ev.Categories, ", "),
			)
		}
	}
}
