#include <iostream>
int64_t incorrect_sum(int64_t a, int64_t b) { return a + b * 2; }
int64_t incorrect_sum(int32_t a, int32_t b) { return (a + b) / 2; }

int main(int argc, char const *argv[])
{
    int a = 32;
    std::cout << "incorrect_sum = " << incorrect_sum(a, a) << std::endl;
}
