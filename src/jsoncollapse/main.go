package main

import (
	"encoding/json"
	"log"
	"os"
)

func main() {
	var v map[string]interface{}
	err := json.NewDecoder(os.Stdin).Decode(&v)
	if err != nil {
		log.Fatalln(err)
	}
	v = merge(v)
	err = json.NewEncoder(os.Stdout).Encode(v)
	if err != nil {
		log.Fatalln(err)
	}
}

func merge(v map[string]interface{}) map[string]interface{} {
	for k := range v {
		array, ok := v[k].([]interface{})
		if ok && len(array) > 0 {
			// TODO: this may fail because array is not guaranteed to contain only dicts
			mergeArrayOfDicts(array)
			v[k] = array[:1]
			continue
		}

		dict, ok := v[k].(map[string]interface{})
		if ok {
			v[k] = merge(dict)
		}
	}
	return v
}

func mergeDicts(a, b map[string]interface{}) map[string]interface{} {
	for k := range b {
		if _, ok := a[k]; !ok {
			a[k] = b[k]
			continue
		}

		if item, ok := b[k].(map[string]interface{}); ok {
			a[k] = mergeDicts(a[k].(map[string]interface{}), item)
		}
		if item, ok := b[k].([]interface{}); ok {
			if item2, ok := a[k].([]interface{}); ok {
				item = append(item, item2...)
				mergeArrayOfDicts(item)
				a[k] = item[:1] // TODO: may fail
			}
		}
	}
	return a
}

func mergeArrayOfDicts(array []interface{}) {
	for _, item := range array {
		item, ok := item.(map[string]interface{})
		if ok {
			sum, ok := array[0].(map[string]interface{})
			if ok {
				array[0] = mergeDicts(sum, item)
			}
		}
	}
}
