package main

import (
	"reflect"
	"testing"
)

func Test_mergeDicts(t *testing.T) {
	type args struct{}
	tests := []struct {
		name string
		a    map[string]interface{}
		b    map[string]interface{}
		want map[string]interface{}
	}{
		{
			a:    map[string]interface{}{},
			b:    map[string]interface{}{},
			want: map[string]interface{}{},
		},
		{
			a: map[string]interface{}{
				"a": 1,
			},
			b: map[string]interface{}{
				"b": 1,
			},
			want: map[string]interface{}{
				"a": 1,
				"b": 1,
			},
		},
		{
			a: map[string]interface{}{
				"a": 1,
				"c": map[string]interface{}{
					"b": 1,
				},
			},
			b: map[string]interface{}{
				"b": 1,
				"c": map[string]interface{}{
					"a": 1,
				},
			},
			want: map[string]interface{}{
				"a": 1,
				"b": 1,
				"c": map[string]interface{}{
					"a": 1,
					"b": 1,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := mergeDicts(tt.a, tt.b); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("mergeDicts() = %v, want %v", got, tt.want)
			}
		})
	}
}
