module gitlab.com/nwwdles/dotfiles/src

go 1.19

require (
	github.com/corona10/goimagehash v1.0.3
	github.com/martinlindhe/subtitles v0.0.0-20210301100346-9aa635346c86
	github.com/mr-tron/base58 v1.2.0
	golang.org/x/time v0.3.0
)

require (
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/magefile/mage v1.10.0 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/sirupsen/logrus v1.8.0 // indirect
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777 // indirect
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68 // indirect
)
