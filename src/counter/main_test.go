package main

import (
	"sync"
	"testing"
)

type Counter struct {
	v int
	c chan struct{}
}

func New() *Counter {
	return &Counter{
		c: make(chan struct{}, 1),
	}
}

func (c *Counter) Inc() {
	c.c <- struct{}{}
	defer func() { <-c.c }()
	c.v++
}

func (c *Counter) Get() int {
	c.c <- struct{}{}
	defer func() { <-c.c }()
	return c.v
}

func TestCounter(t *testing.T) {
	const reps = 1000

	c := New()
	wg := &sync.WaitGroup{}
	for i := 0; i < reps; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			c.Inc()
			_ = c.Get()
		}()
	}
	wg.Wait()

	if v := c.Get(); v != reps {
		t.Errorf("%v != %v", v, reps)
	}
}
