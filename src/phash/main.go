package main

import (
	"flag"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"mime"
	"os"
	"path/filepath"
	"sync"

	"github.com/corona10/goimagehash"
)

func main() {
	flag.Parse()

	o := make(chan string, 1)
	lim := make(chan struct{}, 40)
	wg := sync.WaitGroup{}

	done := make(chan struct{})
	go func() {
		defer close(done)
		i := 0
		for s := range o {
			i++
			if i%100 == 0 {
				fmt.Fprintf(os.Stderr, "processed %v\r", i)
			}
			fmt.Println(s)
		}
	}()
	for _, fname := range flag.Args() {
		h := func(fname string) {
			wg.Add(1)
			lim <- struct{}{}
			go func() {
				defer wg.Done()
				defer func() { <-lim }()
				o <- phash(fname)
			}()
		}

		if v, _ := os.Stat(fname); v.IsDir() {
			de, _ := os.ReadDir(fname)
			for _, d := range de {
				h(filepath.Join(fname, d.Name()))
			}
		} else {
			h(fname)
		}
	}
	wg.Wait()
	close(o)
	<-done
}

func phash(fname string) string {
	var img image.Image
	decode := func(f func(r io.Reader) (image.Image, error)) {
		file1, err := os.Open(fname)
		if err != nil {
			log.Println(err)
			return
		}
		defer file1.Close()

		img, err = f(file1)
		if err != nil {
			log.Println(err)
			return
		}
	}
	switch mime.TypeByExtension(filepath.Ext(fname)) {
	case "image/jpeg":
		decode(jpeg.Decode)
	case "image/png":
		decode(png.Decode)
	default:
		// log.Printf("extension is not supported (file %v)", fname)
		return ""
	}

	avg, err := goimagehash.AverageHash(img)
	if err != nil {
		log.Println(err)
		return ""
	}
	return fmt.Sprintf("%v\t%v", avg.GetHash(), fname)
}
