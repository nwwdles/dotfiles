package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/mr-tron/base58"
)

func main() {
	decode := flag.Bool("d", false, "decode")
	alphabet := flag.String("t", "bitcoin", "type (flickr/bitcoin")
	flag.Parse()

	if *alphabet == "" {
		*alphabet = "bitcoin"
	}

	a := base58.BTCAlphabet
	if (*alphabet)[0] == 'f' {
		a = base58.FlickrAlphabet
	}

	b, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Fatalln(err)
	}

	if *decode {
		out, err := base58.DecodeAlphabet(string(b), a)
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Print(string(out))
		return
	}

	fmt.Print(base58.EncodeAlphabet(b, a))
}
