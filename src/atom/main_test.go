// Находим максимальное четное число

package main

import "testing"

func Test_run(t *testing.T) {
	tests := []struct {
		name string
		in   int
	}{
		{in: 1000},
		{in: 2000},
		{in: 3000},
		{in: 4000},
		{in: 5000},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := runAtomic(tt.in); got != tt.in {
				t.Errorf("run() = %v, want %v", got, tt.in)
			}
		})
	}
}

func Test_runMutex(t *testing.T) {
	tests := []struct {
		name string
		in   int
	}{
		{in: 1000},
		{in: 2000},
		{in: 3000},
		{in: 4000},
		{in: 5000},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := runMutex(tt.in); got != tt.in {
				t.Errorf("run() = %v, want %v", got, tt.in)
			}
		})
	}
}

func Benchmark(b *testing.B) {
	b.Run("atomic", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			v := runAtomic(1000)
			v--
		}
	})
	b.Run("mutex", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			v := runMutex(1000)
			v--
		}
	})
}
