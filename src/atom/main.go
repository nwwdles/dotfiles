// Находим максимальное четное число

package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func runAtomic(n int) int {
	var max atomic.Int64
	var wg sync.WaitGroup

	for i := n; i > 0; i-- {
		i := i
		wg.Add(1)
		go func() {
			defer wg.Done()

			var m int64
			for i%2 == 0 && !max.CompareAndSwap(m, int64(i)) {
				if m = max.Load(); i <= int(m) {
					return
				}
			}
		}()
	}

	wg.Wait()

	return int(max.Load())
}

func runMutex(n int) int {
	var max int
	var wg sync.WaitGroup
	var mu sync.Mutex

	for i := n; i > 0; i-- {
		i := i
		wg.Add(1)
		go func() {
			defer wg.Done()

			mu.Lock()
			defer mu.Unlock()

			if i%2 == 0 && i > max {
				max = i
			}
		}()
	}

	wg.Wait()

	return max
}

func main() {
	fmt.Printf("Maximum is %d", runAtomic(1000))
}
