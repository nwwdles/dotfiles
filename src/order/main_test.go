package main

import (
	"bytes"
	"io"
	"strings"
	"testing"
)

func Test_process(t *testing.T) {
	tests := []struct {
		name  string
		r     io.Reader
		order func(string) int
		wantW string
	}{
		{
			r:     strings.NewReader("foo\nbaz123\nbar\nbaz\n"),
			order: fromslice([]string{"baz", "bar"}),
			wantW: "baz\nbaz123\nbar\nfoo\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := &bytes.Buffer{}
			reorder(tt.r, w, tt.order)
			if gotW := w.String(); gotW != tt.wantW {
				t.Errorf("process() = %v, want %v", gotW, tt.wantW)
			}
		})
	}
}
