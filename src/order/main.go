package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"sort"
	"strings"
)

// slow as hell but i wont be using it with anything too big anyway
func fromslice(dict []string) func(string) int {
	return func(s string) int {
		for i, a := range dict {
			if strings.HasPrefix(strings.TrimSpace(s), a) {
				return i
			}
		}
		return len(dict) + 1
	}
}

func reorder(r io.Reader, w io.Writer, order func(string) int) {
	type line struct {
		line string
		n    int
	}
	br := bufio.NewReader(r)
	var ss []line
	for {
		s, err := br.ReadString('\n')
		if s != "" {
			if s[len(s)-1] != '\n' {
				s += "\n"
			}
			ss = append(ss, line{line: s, n: order(s)})
		}
		if err != nil {
			break
		}
	}
	sort.Slice(ss, func(i, j int) bool {
		if ss[i].n == ss[j].n {
			return ss[i].line < ss[j].line
		}
		return ss[i].n < ss[j].n
	})
	for _, l := range ss {
		fmt.Fprint(w, l.line)
	}
}

func main() {
	flag.Parse()
	reorder(os.Stdin, os.Stdout, fromslice(flag.Args()))
}
