package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	subtitles "github.com/martinlindhe/subtitles"
)

func main() {
	flag.Parse()

	b1, err := os.ReadFile(flag.Args()[0])
	if err != nil {
		panic(err)
	}

	ByteOrderMarkAsString := string('\uFEFF')
	s1 := strings.TrimPrefix(string(b1), ByteOrderMarkAsString)

	res1, err := subtitles.NewFromSRT(s1)
	if err != nil {
		panic(err)
	}

	b2, err := os.ReadFile(flag.Args()[1])
	if err != nil {
		panic(err)
	}
	s2 := strings.TrimPrefix(string(b2), ByteOrderMarkAsString)

	res2, err := subtitles.NewFromSRT(s2)
	if err != nil {
		panic(err)
	}

	m := make(map[string][]string)
	for _, v := range res2.Captions {
		m[v.Start.String()] = v.Text
	}

	for _, v := range res1.Captions {
		fmt.Println(
			strings.ReplaceAll(strings.Join(v.Text, " "), "\t", " "),
			"\t",
			strings.ReplaceAll(strings.Join(m[v.Start.String()], " "), "\t", " "),
		)
	}
}
