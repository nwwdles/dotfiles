#!/bin/sh
set -eu

MAIN_PKGS='
    autoconf
    automake
    binutils
    bison
    fakeroot
    file
    findutils
    flex
    gawk
    gcc
    gettext
    grep
    groff
    gzip
    libtool
    m4
    make
    pacman
    patch
    pkgconf
    sed
    sudo
    texinfo
    which

    appmenu-gtk-module
    arandr
    aspell-en
    aspell-ru
    audacity
    autorandr
    baobab
    barrier
    bash
    bash-language-server
    bleachbit
    blender
    blueman
    borg
    chafa
    chromium
    clang
    copyq
    dbeaver
    dconf-editor
    delve
    digikam
    direnv
    docker
    docker-compose
    emacs
    exa
    extra-cmake-modules
    fd
    filemanager-actions
    firefox
    fzf
    ghc
    gimp
    git
    git-lfs
    gnome-console
    go
    go-tools
    godot
    gparted
    gpick
    gthumb
    hamster-time-tracker
    helm
    hledger-ui
    hledger-web
    htop
    imagemagick
    imv
    inkscape
    inotify-tools
    inxi
    iotop
    iptables-nft
    iputils
    jpegoptim
    jq
    kak-lsp
    kakoune
    kdeconnect
    kdenlive
    keepassxc
    krita
    kubectl
    kubectx
    kvantum-qt5
    lf
    lib32-libcanberra
    libreoffice-still
    libvirt
    libvterm
    lmms
    lua
    minikube
    mpc
    mpd
    mpv
    ncmpcpp
    neofetch
    neovim
    network-manager-applet
    nextcloud-client
    nfs-utils
    noto-fonts-emoji
    npm
    obs-studio
    otf-ipafont
    pandoc
    pasystray
    pavucontrol
    pkgfile
    playerctl
    powertop
    prettier
    pulseaudio-ctl
    python
    python-black
    python-llfuse
    python-pip
    python-pylint
    qbittorrent
    qemu
    qemu-desktop
    qt5ct
    redis
    redshift
    ripgrep
    rofi
    rust
    screenfetch
    shellcheck
    shfmt
    simplescreenrecorder
    spectacle
    sqlitebrowser
    starship
    strace
    sxiv
    syncthing
    tar
    telegram-desktop
    thunderbird
    tldr
    tmux
    trash-cli
    ttf-fira-code
    usbip
    usbutils
    veracrypt
    virtualbox
    wget
    wine-gecko
    wine-mono
    winetricks
    xclip
    xdo
    xdotool
    xorg-xev
    xorg-xkill
    xorg-xsetroot
    xterm
    yarn
    yay
    yt-dlp
    zola
    zsh
'
AUR_PKGS='
    brother-brgenml1
    brscan4
    czkawka-gui-bin
    datagrip
    datagrip-jre
    droidcam
    foobar2000
    frece
    git-extras
    golangci-lint
    insomnia-bin
    jumpapp-git
    lua-format-git
    lua-language-server-git
    mpdris2
    openmpt
    openrgb-git
    qimgv
    rtl88x2bu-dkms-git
    ttf-go-mono-git
    ttf-go-sans-git
    vscodium-bin
    z.lua
    zoom
'
PKGS_TO_REMOVE='
    gnome-wallpapers
    illyria-wallpaper
    manjaro-dynamic-wallpaper
    manjaro-gdm-branding
    manjaro-gdm-theme
    manjaro-gnome-assets
    manjaro-wallpapers-18.0
'

ln_s_home() {
    ln_s "./$1" "$HOME/$1"
}

ln_s() {
    src="$(readlink -f "${1%%/}")"
    target="${2%%/}"

    if [ "$src" = "$(readlink -f "$target")" ]; then
        echo "-- Already linked: $target"
        return
    fi

    if [ ! -e "$src" ]; then
        if [ -e "$target" ]; then
            mv "$target" "$src"
        else
            echo "Cannot link '$src': doesn't exist"
            return 1
        fi
    fi

    if [ -e "$target" ]; then
        rm -rf "$target"
    fi

    mkdir -p "$(dirname "$target")"
    ln -sfn "$src" "$target"
    echo "Linked: $target"
}

ln_s_contents() {
    for f in "${1%%/}"/*; do
        [ -e "$f" ] || return 1 # sh has no nullglob
        ln_s "$f" "${2%%/}/$(basename "$f")"
    done
}

deps() {
    printf '%s\n' "$@" | sed '/^$/d' | while read -r pkg; do
        pactree -u "$pkg" &
    done
    wait
}

singles() {
    awk '{!seen[$0]++};END{for(i in seen) if(seen[i]==1)print i}'
}

install_main_pkgs() {
    if ! pacman -Qq "$@" >/dev/null; then sudo pacman -S --needed --noconfirm "$@"; fi
}

remove_pkgs() {
    if pacman -Qq "$@" >/dev/null 2>&1; then sudo pacman -R "$@"; fi
}

install_aur_pkgs() {
    if ! pacman -Qq "$@" >/dev/null; then yay -Sq -a --noprovides --needed --noredownload --norebuild --nocleanmenu --nodiffmenu "$@"; fi
}

main() {
    if [ "${1-}" = --report ]; then
        printf "main packages not listed in deps:\n\n"
        { deps "$MAIN_PKGS" | tee /dev/stdout && pacman -Qqen; } | singles | sort -u
        echo
        printf "aur packages not listed in deps:\n\n"
        { deps "$AUR_PKGS" | tee /dev/stdout && pacman -Qqem; } | singles | sort -u
        return
    fi

    (
        cd core
        ln_s_contents .local/share/applications "${XDG_DATA_HOME:-$HOME/.local/share}/applications"
        # ln_s_home .tmux.conf
        ln_s_home .bash_profile
        ln_s_home .bashrc
        ln_s_home .clang-format
        ln_s_home .config/alacritty
        ln_s_home .config/aliases.sh
        ln_s_home .config/borg-exclude
        ln_s_home .config/bspwm
        ln_s_home .config/Code/User
        ln_s_home .config/zathura/zathurarc
        ln_s_home .config/copyq
        ln_s_home .config/dunst
        ln_s_home .config/git
        ln_s_home .config/gotests-templates
        ln_s_home .config/imv
        ln_s_home .config/kritashortcutsrc
        ln_s_home .config/luaformatter
        ln_s_home .config/mpd
        ln_s_home .config/mpv
        ln_s_home .config/nvim
        ln_s_home .config/newsboat/urls
        ln_s_home .config/polybar
        ln_s_home .config/pulseaudio-ctl
        ln_s_home .config/rofi
        ln_s_home .config/starship.toml
        ln_s_home .config/sxhkd
        ln_s_home .config/sxiv
        ln_s_home .config/youtube-dl
        ln_s_home .editorconfig
        ln_s_home .golangci.yml
        ln_s_home .local/share/rofi
        ln_s_home .profile
        ln_s_home .vimrc
        ln_s_home .Xresources
        ln_s_home .zshrc
        ln_s_home bin
    )

    mkdir -p "${XDG_DATA_HOME:-$HOME/.local/share}/mpd"

    ln_s_contents core/.config/systemd/user "${XDG_CONFIG_HOME:-$HOME/.config}/systemd/user"

    mkdir -p "$HOME/.ssh"
    if command -v systemctl >/dev/null; then
        systemctl --user enable ssh-agent
        systemctl --user start ssh-agent
    fi

    config_dir="$HOME/.config/Code"
    extensions_dir="$HOME/.vscode"
    mkdir -p "$extensions_dir" "$config_dir"
    ln_s "$extensions_dir" "$HOME/.vscode-oss"
    ln_s "$config_dir" "$HOME/.config/Code - OSS"
    ln_s "$config_dir" "$HOME/.config/VSCodium"

    remove_pkgs $PKGS_TO_REMOVE
    install_main_pkgs $MAIN_PKGS
    install_aur_pkgs $AUR_PKGS

    chmod +x "core/bin/vscode-patch-css.sh"
    "core/bin/vscode-patch-css.sh" || true

    echo 'Done!'
}

main "$@"
