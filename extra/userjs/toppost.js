// ==UserScript==
// @name        topposts
// @namespace   top
// @description top posts in the thread
// @include     http*://boards.4chan.org/*
// @include     http*://boards.4channel.org/*
// @version     0.0.1
// @run-at      document-end
// ==/UserScript==

function parseOriginalPosts() {
  var tempAllPostsOnPage = document.getElementsByClassName("postContainer");
  postContainers = Array.prototype.slice.call(tempAllPostsOnPage); //convert from element list to javascript array
  postContainers.forEach(function (postContainer) {
    processPostContainer(postContainer);
  });
}

document.addEventListener(
  "ThreadUpdate",
  function (e) {
    var evDetail = e.detail || e.wrappedJSObject.detail;
    var evDetailClone =
      typeof cloneInto === "function"
        ? cloneInto(evDetail, unsafeWindow)
        : evDetail;

    //ignore if 404 event
    if (evDetail[404] === true) {
      return;
    }

    setTimeout(function () {
      evDetailClone.newPosts.forEach(function (post_board_nr) {
        var post_nr = post_board_nr.split(".")[1];
        var newPostDomElement = document.getElementById("pc" + post_nr);
        processPostContainer(newPostDomElement);
      });
    }, 5000);
  },
  false
);

document.addEventListener(
  "4chanThreadUpdated",
  function (e) {
    var evDetail = e.detail || e.wrappedJSObject.detail;

    setTimeout(function () {
      var threadID = window.location.pathname.split("/")[3];
      var postsContainer = Array.prototype.slice.call(
        document.getElementById("t" + threadID).childNodes
      );
      var lastPosts = postsContainer.slice(
        Math.max(postsContainer.length - evDetail.count, 1)
      ); //get the last n elements (where n is evDetail.count)

      lastPosts.forEach(function (post_container) {
        processPostContainer(post_container);
      });
    }, 5000);
  },
  false
);

function processPostContainer(postContainer) {
  var l = postContainer
    .getElementsByClassName("postInfo")[0]
    .getElementsByClassName("backlink");
  if (l.length < 3) {
    return;
  }
  var post = postContainer.getElementsByClassName("post")[0];
  var createA = document.createElement("a");
  var createAText = document.createTextNode(
    ">>" + post.id.substring(1) + " (" + l.length + ")"
  );
  createA.setAttribute("href", "#" + post.id);
  createA.appendChild(createAText);
  createA.setAttribute("class", "quotelink");
  var createbr = document.createElement("br");
  var div = document.getElementById("top-replies-container");
  div.appendChild(createbr);
  div.appendChild(createA);
}

function init() {
  var textSpan = document.createElement("SPAN");
  textSpan.setAttribute("id", "top-thread-replies-text");
  textSpan.textContent = "Top: ";

  var span = document.createElement("SPAN");
  span.setAttribute("id", "top-thread-replies");
  span.appendChild(textSpan);

  var div = document.createElement("DIV");
  div.setAttribute("id", "top-replies-container");
  div.setAttribute("class", "top-replies-container dialog");
  div.style.position = "fixed";
  div.style.bottom = 0;
  div.style.right = 0;
  div.style["overflow-y"] = "scroll";
  div.style["max-height"] = "300px";
  div.appendChild(span);

  var body = document.getElementsByTagName("BODY")[0];
  body.appendChild(div);
}

setTimeout(init, 2500);
setTimeout(parseOriginalPosts, 5000);
