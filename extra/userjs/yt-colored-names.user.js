// ==UserScript==
// @name        yt-colored-names
// @namespace   Violentmonkey Scripts
// @grant       none
// @version     1.0
// @author      -
// @description 12/26/2020, 9:47:32 PM
// @include     https://www.youtube.com/*
// @include     https://www.youtube.com/embed/*
// @include     https://www.youtube-nocookie.com/embed/*
// ==/UserScript==

(() => {
    let defaultColors = [
        "#FF0000",
        "#00FF00",
        "#B22222",
        "#FF7F50",
        "#9ACD32",
        "#FF4500",
        "#2E8B57",
        "#DAA520",
        "#D2691E",
        "#5F9EA0",
        "#1E90FF",
        "#FF69B4",
        "#8A2BE2",
        "#00FF7F",
    ];

    function getUserChatColor(str) {
        str = str.toLowerCase().trim();
        let hash = 0;
        for (let i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }

        return defaultColors[Math.abs(hash % defaultColors.length)];
    }

    const YouTube = {
        handleMessage(node) {
            let author = node.querySelector("#author-name");
            if (author) {
                author.style.color = getUserChatColor(author.innerText);
            }
        },
        init() {
            const chatQuerySelector = "#items.yt-live-chat-item-list-renderer";
            const init = (documentElement, target) => {
                if (target !== null) {
                    this.document = documentElement;

                    const observer = new MutationObserver((mutations) => {
                        for (let mutation of mutations) {
                            for (let node of mutation.addedNodes) {
                                this.handleMessage(node);
                            }
                        }
                    });

                    for (let element of this.document.querySelectorAll(
                        "yt-live-chat-text-message-renderer"
                    )) {
                        this.handleMessage(element);
                    }

                    const config = {
                        attributes: true,
                        childList: true,
                        characterData: true,
                    };
                    observer.observe(target, config);
                }
            };

            let target = document.querySelector(chatQuerySelector);
            if (target === null) {
                let interval = setInterval(() => {
                    let chatFrame = document.querySelector("#chatframe");
                    if (chatFrame) {
                        let documentElement = chatFrame.contentDocument;
                        target = documentElement.querySelector(
                            chatQuerySelector
                        );

                        if (target !== null) {
                            clearInterval(interval);
                            init(documentElement, target);
                        }
                    }
                }, 250);
            } else {
                init(document, target);
            }
        },
    };
    if (location.hostname.toLowerCase().includes(".")) {
        YouTube.init();
    } else {
        document.addEventListener("DOMContentLoaded", YouTube.init);
    }
})();
